<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Form\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\AbstractHelper;
use ZF2x\Form\Element;

class CloudinaryDirectFileUpload extends AbstractHelper
{

    /**
     * Invoke helper as functor
     *
     * Proxies to {@link render()}.
     *
     * @param  ElementInterface|null $element
     * @param  null|string           $buttonContent
     * @return string|FormButton
     */
    public function __invoke(ElementInterface $element = null)
    {
        return $this->render($element);
    }

    /**
     * [render description]
     * @param  ElementInterface $element [description]
     * @return [type]                    [description]
     */
    public function render(ElementInterface $element)
    {
        if (!$element instanceof Element\CloudinaryDirectFileUpload) {
            throw new \Exception("invalid element type");
        }
        if (array_key_exists('REQUEST_SCHEME', $_SERVER)) {
            $cors_location = $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["SERVER_NAME"] .
            dirname($_SERVER["SCRIPT_NAME"]) . "/cloudinary_cors.html";
        } else {
            $cors_location = "http://" . $_SERVER["HTTP_HOST"] . "/cloudinary_cors.html";
        }
        $html = cl_image_upload_tag(
            $element->getName(),
            array(
                "callback"  => $cors_location,
                "crop"      => "limit",
                "width"     => 350,
                "height"    => 100,
            )
        );
        $imgUrl = '';
        if ($element->getValue()) {
            try {
                $preloaded = new \Cloudinary\PreloadedFile($element->getValue());
                $imgUrl = cloudinary_url($preloaded->identifier());
            } catch (\Exception $e) {
                $imgUrl = false;
            }
        }
        $html .= '<br/><img id="imguppreview" src="'.$imgUrl.'"/>';
        $html .= '<input type="hidden" name="'.$element->getName().'" value="'.$element->getValue().'">';
        return $html;
    }
}
