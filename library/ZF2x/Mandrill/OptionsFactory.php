<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */
namespace ZF2x\Mandrill;

use ZF2x\Stdlib\AbstractOptionsFactory;

class OptionsFactory extends AbstractOptionsFactory
{
    /**
     * @var string FQCN of Options to hydrate
     */
    protected $optionsClass = 'ZF2x\Mandrill\Options';

    /**
     * @var string Path from which to retrieve configuration
     */
    protected $configurationPath = 'zf2x.mandrill.options';

    /**
     * Path from which to retrieve configuration
     *
     * @const string Configuration Path
     */
    const CONFIG_PATH = 'zf2x.mandrill.options';

    /**
     *
     */
    const OPTIONS_CLASS = 'ZF2x\Mandrill\Options';
}
