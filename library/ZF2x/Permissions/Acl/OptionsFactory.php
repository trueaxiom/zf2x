<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl;

use ZF2x\Stdlib\ArrayUtils;
use ZF2x\Stdlib\AbstractOptionsFactory;

/**
 * Factory for Acl Options
 */
class OptionsFactory extends AbstractOptionsFactory
{
    /**
     * Path from which to retrieve configuration
     *
     * @const string Configuration Path
     */
    const CONFIG_PATH = 'permissions.acl';

    /**
     * [createOptions description]
     * @param  array  $config [description]
     * @return [type]         [description]
     */
    public function createOptions(array $config, \Zend\ServiceManager\ServiceLocatorInterface $serviceManager)
    {
        $options = new Options;
        if ($config) {
            $options->setRoles(ArrayUtils::getPath($config, 'roles', null, array()));
            $options->setResources(ArrayUtils::getPath($config, 'resources', null, array()));
            $options->setDeny($this->mapRules(ArrayUtils::getPath($config, 'deny', null, array())));
            $options->setAllow($this->mapRules(ArrayUtils::getPath($config, 'allow', null, array())));
        }
        return $options;
    }

    /**
     * Map rule config
     *
     * @param  array $config config to map
     * @return array()
     */
    protected function mapRules(array $config)
    {
        $map = array();
        foreach ($config as $role => $resources) {
            foreach ($resources as $resource => $rules) {
                $mapped = array();
                $func = function ($rule) use ($role, $resource) {
                    if (!is_array($rule)) {
                        return array($role, $resource, $rule, null);
                    }
                    if (empty($rule)) {
                        return array($role, $resource, array(), null);
                    }
                    if (is_array($rule)&&array_keys($rule) === range(0, count($rule) - 1)) {
                        return array($role, $resource, $rule, null);
                    }
                    return array(
                        $role,
                        $resource,
                        isset($rule['privileges']) ? $rule['privileges'] : array(),
                        isset($rule['assertion']) ? $rule['assertion'] : null
                    );
                };
                if (array_keys($rules) === range(0, count($rules) - 1)) {
                    foreach ($rules as $k => $rule) {
                        if (!is_array($rule)) {
                            $mapped[] = $rule;
                        } else {
                            $map[] = $func($rule);
                        }
                    }
                    if (count($mapped)) {
                        $map[] = $func($mapped);
                    }
                } else {
                    $map[] = $func($rules);
                }
            }
        }
        return $map;
    }
}
