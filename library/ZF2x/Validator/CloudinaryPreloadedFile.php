<?php
namespace ZF2x\Validator;

use Zend\Validator\ValidatorInterface;

class CloudinaryPreloadedFile implements ValidatorInterface
{
    public function isValid($value)
    {
        try {
            $preloaded = new \Cloudinary\PreloadedFile($value);
            $valid = $preloaded->is_valid();
        } catch (\Exception $e) {
            $valid = false;
        }
        return $valid;
    }

    public function getMessages()
    {
        return array('invalid signature');
    }
}
