<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\ORM\Doctrine;

use ZF2x\ORM\Doctrine\DoctrineORMAwareInterface;
use Zend\ServiceManager\InitializerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * Doctrine ORM Aware Initializer
 */
class DoctrineORMAwareInitializer implements InitializerInterface
{
    /**
     * Initialize
     * @param  mixed                  $instance Instance Supplied
     * @param  ServiceLocatorInterface $locator   Service Locator
     * @return null
     */
    public function initialize($instance, ServiceLocatorInterface $locator)
    {
        if ($instance instanceof DoctrineORMAwareInterface) {
            if (get_class($locator) != "Zend\ServiceManager\ServiceManager") {
                $locator = $locator->getServiceLocator();
            }
            $instance->setEntityManager($locator->get('DoctrineEntityManager'));
        }
    }
}
