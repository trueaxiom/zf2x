<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Authentication;

use Zend\Crypt\Password\Bcrypt;
use Zend\EventManager\EventInterface;
use ZF2x\Identification\Event\IdentityEvent;
use ZF2x\Authentication\Adapter\PasswordAdapter;
use ZF2x\Authentication\Mapper\AuthEntityMapper;
use ZF2x\Authentication\Identification\Manager\AuthIdentityManager;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Authentication\Validator\Authentication as Validator;

class Module implements ConfigProviderInterface, BootstrapListenerInterface
{
    public function onBootstrap(EventInterface $event)
    {
        $app = $event->getApplication();
        $sl = $app->getServiceManager();
        $em = $app->getEventManager();
        $sm = $em->getSharedManager();

        $sm->attach(
            '*',
            IdentityEvent::IDENTIFY_EVENT,
            function ($e) use ($sl) {
                $auth = $sl->get('AuthIdentityManager');
                return $auth->onIdentify($e);
            }
        );

        $sm->attach(
            '*',
            IdentityEvent::UPDATE_EVENT,
            function ($e) use ($sl) {
                $auth = $sl->get('AuthIdentityManager');
                return $auth->onUpdate($e);
            }
        );

        $sm->attach(
            '*',
            IdentityEvent::CLEAR_EVENT,
            function ($e) use ($sl) {
                $auth = $sl->get('AuthIdentityManager');
                return $auth->onClear($e);
            }
        );
    }

    public function getConfig()
    {
        return array(
            'service_manager' => array(
                'invokables' => array(
                    'PasswordService' => 'Zend\Crypt\Password\Bcrypt',
                    'AuthenticationService' => 'ZF2x\Authentication\AuthenticationService',
                ),
                'factories' => array(
                    'AuthIdentityMapper' => function ($sl) {
                        return new AuthEntityMapper;
                    },
                    'AuthIdentityManager' => function ($sl) {
                        $manager = new AuthIdentityManager;
                        $mapper = $sl->get('AuthEntityMapper');
                        $manager->setMapper($mapper);
                        return $manager;
                    },
                    'AuthenticationAdapter' => function ($sl) {
                        $adapter = new PasswordAdapter;
                        $mapper = $sl->get('AuthEntityMapper');
                        $adapter->setMapper($mapper);
                        $adapter->setIdentity('identity');
                        $adapter->setCredential('credential');
                        $passwordService = $sl->get('PasswordService');
                        $adapter->setPasswordService($passwordService);
                        return $adapter;
                    }
                ),
            ),
            'validators' => array(
                'factories' => array(
                    'AuthValidator' => function ($sl) {
                        $sm = $sl->getServiceLocator();
                        $validator = new Validator;
                        $validator->setIdentity('identity');
                        $validator->setCredential('credential');
                        $service = $sm->get('AuthenticationService');
                        $adapter = $sm->get('AuthenticationAdapter');
                        $validator->setService($service);
                        $validator->setAdapter($adapter);
                        return $validator;
                    }
                ),
            ),
        );
    }
}
