<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\ORM\Doctrine\Factory;

use Doctrine\ORM\Configuration;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Doctrine ORM Configuration Factory
 */
class ConfigurationFactory implements FactoryInterface
{
    /**
     * Create Service
     * @param  ServiceLocatorInterface $serviceLocator Service Locator
     * @return Configuration  Doctrine ORM Configuration
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $configurationOptions = $serviceLocator->get('DoctrineORMConfigurationOptions');
        $configuration = new Configuration;
        $cache = $configurationOptions->getMetadataCache();
        $configuration->setMetadataCacheImpl(new $cache);
        $cache = $configurationOptions->getQueryCache();
        $configuration->setQueryCacheImpl(new $cache);
        $cache = $configurationOptions->getResultCache();
        $configuration->setResultCacheImpl(new $cache);
        $configuration->setProxyDir($configurationOptions->getProxyDirectory());
        $configuration->setProxyNamespace($configurationOptions->getProxyNamespace());
        $configuration->setAutoGenerateProxyClasses($configurationOptions->getGenerateProxies());
        $configuration->setMetadataDriverImpl(
            $configuration->newDefaultAnnotationDriver($configurationOptions->getEntityPaths(), true)
        );
        return $configuration;
    }
}
