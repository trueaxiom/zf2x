<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Mail;

use Zend\Stdlib\AbstractOptions;

class Options extends AbstractOptions
{
    /**
     * Default From Name
     *
     * @var string
     */
    protected $defaultFromName;

    /**
     * Default From Email Address
     *
     * @var string
     */
    protected $defaultFromEmailAddress;
    /**
     * Default Transport
     *
     * @var string ServiceManager Alias
     */
    protected $defaultTransport;

    /**
     * If set, use a View Renderer pulled from the Service Manager with specified alias
     *
     * @var string Service Manager Alias
     */
    protected $defaultRenderer;

    /**
     * Gets the Default From Name.
     *
     * @return string
     */
    public function getDefaultFromName()
    {
        return $this->defaultFromName;
    }

    /**
     * Sets the Default From Name.
     *
     * @param string $defaultFromName the default from name
     *
     * @return self
     */
    public function setDefaultFromName($defaultFromName)
    {
        $this->defaultFromName = $defaultFromName;

        return $this;
    }

    /**
     * Gets the Default From Email Address.
     *
     * @return string
     */
    public function getDefaultFromEmailAddress()
    {
        return $this->defaultFromEmailAddress;
    }

    /**
     * Sets the Default From Email Address.
     *
     * @param string $defaultFromEmailAddress the default from email address
     *
     * @return self
     */
    public function setDefaultFromEmailAddress($defaultFromEmailAddress)
    {
        $this->defaultFromEmailAddress = $defaultFromEmailAddress;

        return $this;
    }

    /**
     * Gets the Default Transport.
     *
     * @return string ServiceManager Alias
     */
    public function getDefaultTransport()
    {
        return $this->defaultTransport;
    }

    /**
     * Sets the Default Transport.
     *
     * @param string ServiceManager Alias $defaultTransport the default transport
     *
     * @return self
     */
    public function setDefaultTransport($defaultTransport)
    {
        $this->defaultTransport = $defaultTransport;

        return $this;
    }

    /**
     * Gets the If set, use a View Renderer pulled from the Service Manager with specified alias.
     *
     * @return string Service Manager Alias
     */
    public function getDefaultRenderer()
    {
        return $this->defaultRenderer;
    }

    /**
     * Sets the If set, use a View Renderer pulled from the Service Manager with specified alias.
     *
     * @param string Service Manager Alias $defaultRenderer the default renderer
     *
     * @return self
     */
    public function setDefaultRenderer($defaultRenderer)
    {
        $this->defaultRenderer = $defaultRenderer;

        return $this;
    }
}
