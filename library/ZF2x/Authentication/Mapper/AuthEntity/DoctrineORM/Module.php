<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Authentication\Mapper\AuthEntity\DoctrineORM;

class Module
{
    public function getConfig()
    {
        return array(
            'service_manager' => array(
                'factories' => array(
                    'AuthEntityDoctrineORMOptions'=>'ZF2x\Authentication\Mapper\AuthEntity\DoctrineORM\OptionsFactory',
                    'AuthEntityMapper' => function ($sl) {
                        $options = $sl->get('AuthEntityDoctrineORMOptions');
                        $entityManager = $sl->get('DoctrineEntityManager');
                        $mapper = $entityManager->getRepository($options->getEntityClass());
                        return $mapper;
                    },
                ),
            )
        );
    }
}
