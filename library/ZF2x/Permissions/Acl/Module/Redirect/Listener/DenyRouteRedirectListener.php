<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Module\Redirect\Listener;

use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use Zend\Stdlib\AbstractOptions;
use Zend\EventManager\EventManager;
use ZF2x\Permissions\Acl\Event\AclEvent;
use Zend\EventManager\EventManagerInterface;

/**
 * Listen for Deny Route Event and return a redirect response if configured
 */
class DenyRouteRedirectListener
{
    /**
     * Event Manager
     * @var EventManagerInterface
     */
    protected $eventManager;

    /**
     * Acl Options
     * @var \Zend\Stdlib\AbstractOptions
     */
    protected $options;

    /**
     * On Deny Route
     * @param  MvcEvent $e Event
     * @return Response
     */
    public function onDenyRoute(MvcEvent $e)
    {
        $routeMatch = $e->getRouteMatch();
        $resource   = $routeMatch->getParam('controller');
        $privilege  = $routeMatch->getParam('action');
        $options    = $this->getOptions();
        $redirects  = $options->getRedirect();
        $identities = $this->getEventManager()->trigger(AclEvent::IDENTIFY_EVENT);
        $redirect   = false;
        //might need to be more sophisticated utilizing weights or some such
        foreach ($identities as $identity) {
            $role = $identity->getRole();
            if (isset($redirects[$role])) {
                $redirect = $redirects[$role];
                if (isset($redirect[$resource])) {
                    $redirect = $redirect[$resource];
                    if (isset($redirect[$privilege])) {
                        $redirect = $redirect[$privilege];
                    }
                }
            }
        }
        if ($redirect && isset($redirect['route'])) {
            $route = $redirect['route'];
            $params = isset($redirect['params']) ? $redirect['params'] : array();
            $router = $e->getRouter();
            $url = $router->assemble($params, array('name' => $route));
            $response = $e->getResponse();
            $response->getHeaders()->addHeaderLine('Location', $url);
            $response->setStatusCode(302);
            $e->stopPropagation();
            return $response;
        }
    }

    /**
     * Gets the Event Manager.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->eventManager) {
            $this->setEventManager(new EventManager);
        }
        return $this->eventManager;
    }

    /**
     * Sets the Event Manager.
     *
     * @param EventManagerInterface $eventManager the eventManager
     *
     * @return self
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->eventManager = $eventManager;

        return $this;
    }

    /**
     * Gets the value of options.
     *
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Sets the value of options.
     *
     * @param mixed $options the options
     *
     * @return self
     */
    public function setOptions(AbstractOptions $options)
    {
        $this->options = $options;

        return $this;
    }
}
