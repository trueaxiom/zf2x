<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Mail\Transport;

use Zend\Mail\Message;
use Zend\Mail\Transport\TransportInterface as ZendTransportInterface;

interface TransportInterface extends ZendTransportInterface
{
    /**
     * Send a mail message
     *
     * @param ZF2x\Mail\Message
     * @return
     */
    public function send(Message $message);
}
