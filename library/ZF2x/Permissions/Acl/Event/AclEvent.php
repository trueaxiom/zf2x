<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Event;

use Zend\EventManager\Event;

class AclEvent extends Event
{
    const IDENTIFY_EVENT    = 'acl.identify';
    const DENY_ROUTE_EVENT  = 'acl.deny.route';
    const UPDATE_EVENT      = 'update.acl';
    const IS_ALLOWED_EVENT  = 'acl.is.allowed';

    /**
     * @var string
     */
    protected $resource;

    /**
     * @var string
     */
    protected $privilege;

    /**
     * @var string
     */
    protected $role;

    /**
     * Gets the value of resource.
     *
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Sets the value of resource.
     *
     * @param string $resource the resource
     *
     * @return self
     */
    public function setResource($resource)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Gets the value of privilege.
     *
     * @return string
     */
    public function getPrivilege()
    {
        return $this->privilege;
    }

    /**
     * Sets the value of privilege.
     *
     * @param string $privilege the privilege
     *
     * @return self
     */
    public function setPrivilege($privilege)
    {
        $this->privilege = $privilege;

        return $this;
    }

    /**
     * Gets the value of role.
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Sets the value of role.
     *
     * @param string $role the role
     *
     * @return self
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }
}
