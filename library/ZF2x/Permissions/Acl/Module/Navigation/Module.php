<?php
/**
 * True Axiom Framework (http://framework.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/framework for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Module\Navigation;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventInterface;
use ZF2x\Permissions\Acl\Event\AclEvent;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;

/**
 * Requires ZF2x\Permissions\Acl Module
 * @author Richard Jennings <rich@trueaxiom.co.uk>
 */
class Module implements BootstrapListenerInterface
{
    public function onBootstrap(EventInterface $event)
    {
        $app = $event->getApplication();
        $sm = $app->getServiceManager();
        $events = $app->getEventManager();
        $events->getSharedManager()->attach(
            'Zend\View\Helper\Navigation\AbstractHelper',
            'isAllowed',
            function ($e) use ($sm) {
                $e->stopPropagation();
                $page       = $e->getParam('page');
                if (get_class($page) === 'ZF2x\Navigation\Page\ZF2xPage') {
                    $resource   = new GenericResource($page->getResource());
                    $privilege  = $page->getPrivilege();
                } else {
                    $resource = new GenericResource($page->getParam('resource'));
                    $privilege = $page->getParam('privilege');
                }
                $event      = new AclEvent;
                $event->setResource($resource);
                $event->setPrivilege($privilege);
                $eventManager = new EventManager;
                $allowed = $eventManager->trigger(AclEvent::IS_ALLOWED_EVENT, $event)->last();
                return $allowed;
            }
        );
    }
}
