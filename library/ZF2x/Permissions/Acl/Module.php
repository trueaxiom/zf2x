<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use ZF2x\Permissions\Acl\Event\AclEvent;
use ZF2x\Identification\Event\IdentityEvent;
use ZF2x\Permissions\Acl\Identity\AclIdentityManager;
use ZF2x\Permissions\Acl\Listener\DenyRouteRedirectListener;

/**
 * Acl Module
 */
class Module implements ConfigProviderInterface, BootstrapListenerInterface
{
    public function onBootstrap(EventInterface $event)
    {
        $app = $event->getApplication();
        $sl = $app->getServiceManager();
        $em = $app->getEventManager();
        $sm = $em->getSharedManager();
        $aclListener = $sl->get('AclListener');
        //On Allowed Event
        $sm->attach('*', AclEvent::IS_ALLOWED_EVENT, array($aclListener, 'onIsAllowed'));
        //General Identify Event
        $sm->attach('*', IdentityEvent::IDENTIFY_EVENT, array($sl->get('AclIdentityManager'), 'onIdentify'));
        //ACL Identify Event
        $sm->attach('*', AclEvent::IDENTIFY_EVENT, array($sl->get('AclIdentityManager'), 'onIdentify'));
        //ACL Identity Update Event
        $sm->attach('*', AclEvent::UPDATE_EVENT, array($sl->get('AclIdentityManager'), 'onUpdate'));
        //General Identity Update Event
        $sm->attach(
            '*',
            IdentityEvent::UPDATE_EVENT,
            function ($e) use ($sl) {
                $manager = $sl->get('AclIdentityManager');
                return $manager->onUpdate($e);
            }
        );

        //General Identity Update Event
        $sm->attach('*', IdentityEvent::CLEAR_EVENT, array($sl->get('AclIdentityManager'), 'onClear'));
    }

    public function getConfig()
    {
        return array(
            'service_manager' => array(
                'factories' => array(
                    'AclListener' => function ($sl) {
                        $service = new Listener\AclListener;
                        $acl = $sl->get('Acl');
                        $service->setAcl($acl);
                        return $service;
                    },
                    'AclIdentityManager' => function ($sl) {
                        $manager = new AclIdentityManager;
                        $options = $sl->get('AclOptions');
                        $manager->setOptions($options);
                        $manager->setMapper($sl->get('AuthRoleEntityMapper'));
                        return $manager;
                    },
                    'Acl' => 'ZF2x\Permissions\Acl\Factory',
                    'AclOptions'=> 'ZF2x\Permissions\Acl\OptionsFactory',
                ),
            ),
        );
    }
}
