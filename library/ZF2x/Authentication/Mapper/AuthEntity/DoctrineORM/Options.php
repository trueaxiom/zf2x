<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Authentication\Mapper\AuthEntity\DoctrineORM;

use Zend\Stdlib\AbstractOptions;

class Options extends AbstractOptions
{
    /**
     * Identities
     * @var array
     */
    protected $entityClass;

    /**
     * Gets the Identities.
     *
     * @return array
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    /**
     * Sets the Identities.
     *
     * @param array $entityClass the entity class
     *
     * @return self
     */
    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;

        return $this;
    }
}
