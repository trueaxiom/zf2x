<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Stdlib;

use ZF2x\Stdlib\ArrayUtils;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Factory for Acl Options
 */
abstract class AbstractOptionsFactory implements FactoryInterface
{
    /**
     * Path from which to retrieve configuration
     *
     * @const string Configuration Path
     */
    const CONFIG_PATH = null;

    /**
     *
     */
    const OPTIONS_CLASS = null;

    /**
     *
     */
    const CONFIG_DELIMITER = '.';

    /**
     * Create an Options Instance
     * @param  ServiceLocatorInterface $serviceLocator Service Locator
     * @return AbstractOptions                         The Options
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = ArrayUtils::getPath(
            $serviceLocator->get('config'),
            static::CONFIG_PATH,
            static::CONFIG_DELIMITER,
            array()
        );
        return $this->createOptions($config, $serviceLocator);
    }

    /**
     * Create Options - overide for extended behaviour
     * @param  array                    $config         Configuration
     * @param  ServiceLocatorInterface  $serviceLocator Service Locator
     * @return AbstractOptions                  Options
     */
    public function createOptions(array $config, ServiceLocatorInterface $serviceLocator)
    {
        $optionsClass = static::OPTIONS_CLASS;
        $hydrator = new ClassMethods;
        return $hydrator->hydrate($config, new $optionsClass);
    }
}
