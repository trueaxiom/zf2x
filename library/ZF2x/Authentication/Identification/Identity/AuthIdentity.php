<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Authentication\Identification\Identity;

use ZF2x\Identification\Entity\AuthEntity;

class AuthIdentity implements AuthIdentityInterface
{
    /**
     * @var mixed
     */
    protected $authId;

    /**
     * Gets the value of authId.
     *
     * @return mixed
     */
    public function getAuthId()
    {
        return $this->authId;
    }

    /**
     * Sets the value of authId.
     *
     * @param mixed $authId the auth id
     *
     * @return self
     */
    public function setAuthId($authId)
    {
        $this->authId = $authId;

        return $this;
    }
}
