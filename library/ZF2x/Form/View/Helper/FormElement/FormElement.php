<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Form\View\Helper\FormElement;

use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormElement as FormElementHelper;

class FormElement extends FormElementHelper
{
    protected $options;

    public function render(ElementInterface $element)
    {
        $renderer = $this->getView();
        if (!method_exists($renderer, 'plugin')) {
            // Bail early if renderer is not pluggable
            return '';
        }
        $options = $this->getOptions();
        foreach ($options->getMappings() as $namespace => $plugin) {
            if ($element instanceof $namespace) {
                $helper = $renderer->plugin($plugin);
                return $helper($element);
            }
        }
        return parent::render($element);
    }

    /**
     * Gets the value of options.
     *
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Sets the value of options.
     *
     * @param mixed $options the options
     *
     * @return self
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }
}
