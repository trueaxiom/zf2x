<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Module\Routing;

use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use ZF2x\Permissions\Acl\Event\AclEvent;
use ZF2x\Identification\Event\IdentityEvent;
use ZF2x\Permissions\Acl\Identity\AclIdentityManager;
use ZF2x\Permissions\Acl\Listener\DenyRouteRedirectListener;

/**
 * Acl Router Integration Module
 */
class Module implements ConfigProviderInterface, BootstrapListenerInterface
{
    public function onBootstrap(EventInterface $e)
    {
        $application    = $e->getApplication();
        $sl             = $application->getServiceManager();
        $em             = $application->getEventManager();
        $sm             = $em->getSharedManager();

        //Route Event Listener
        $em->attach(
            'route',
            function ($e) use ($sl) {
                $routeMatch = $e->getRouteMatch();
                $resource   = new GenericResource($routeMatch->getParam('controller'));
                $privilege  = $routeMatch->getParam('action');
                $event      = new AclEvent;
                $event->setResource($resource);
                $event->setPrivilege($privilege);
                $eventManager = new EventManager;
                $allowed = $eventManager->trigger(AclEvent::IS_ALLOWED_EVENT, $event)->last();
                if (!$allowed) {
                    return $eventManager->trigger(AclEvent::DENY_ROUTE_EVENT, $e)->last();
                }
            },
            -1000
        );

        //Deny Route Event Listener
        $sm->attach(
            '*',
            AclEvent::DENY_ROUTE_EVENT,
            function ($e) use ($sl) {
                $listener = $sl->get('AclDenyRouteListener');
                return $listener->onDenyRoute($e);
            }
        );
    }

    public function getConfig()
    {
        return array(
            'service_manager' => array(
                'invokables' => array(
                    'AclDenyRouteListener'=>'ZF2x\Permissions\Acl\Module\Routing\Listener\DenyRouteListener',
                ),
            ),
        );
    }
}
