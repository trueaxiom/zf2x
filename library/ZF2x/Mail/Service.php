<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Mail;

use Exception;
use ZF2x\Mail\Message;
use Zend\Mail\AddressList;
use ZF2x\Mail\ServiceInterface;
use Zend\View\Renderer\PhpRenderer;
use Zend\Mail\Message as ZendMailMessage;
use Zend\View\Resolver\TemplatePathStack;
use Zend\View\Renderer\RendererInterface;
use Zend\Mail\Transport\TransportInterface;
use ZF2x\Mail\Transport\TransportInterface as ZF2xTransportInterface;

/**
 * Email Service
 */
class Service implements ServiceInterface
{
    /**
     * Default From Address
     *
     * @var Zend\Mail\AddressList
     */
    protected $defaultFromAddress;

    /**
     * Default Renderer for View Model Rendering
     *
     * @var Zend\View\Renderer\RendererInterface
     */
    protected $defaultRenderer;

    /**
     * Default Transport
     *
     * @var TransportInterface
     */
    protected $defaultTransport;

    /**
     * Create a Message
     *
     * @return Message
     */
    public function createMessage(AddressList $toAddress = null)
    {
        $message = new Message;
        if ($toAddress) {
            $message->setTo($toAddress);
        }
        $renderer = $this->getDefaultRenderer();
        if ($renderer) {
            $message->setRenderer($renderer);
        }
        $from = $this->getDefaultFromAddress();
        if ($from) {
            $message->setFrom($from);
        }
        return $message;
    }

    /**
     * Send A Message
     *
     * @param  Message              $message
     * @param  TransportInterface   $transport
     *
     * @return self
     */
    public function sendMessage(ZendMailMessage $message, TransportInterface $transport = null)
    {
        if (null === $transport) {
            $transport = $this->getDefaultTransport();
            if (!$transport) {
                throw new Exception("No Transport Specified");
            }
        }
        if ($transport instanceof ZF2xTransportInterface) {
            if ($message instanceof ZendMailMessage) {
                $message->prepare();
            }
        } else {
            $message = $this->baseMessageToZF2xMessage($message);
        }
        $transport->send($message);

        return $this;
    }

    /**
     * Receive a Message
     *
     * @param  Message $message
     * @return null
     */
    public function receiveMessage(ZendMailMessage $message)
    {
    }

    /**
     * Compatibility with Zend Mail Message Class
     *
     * @param  ZendMailMessage      $message
     *
     * @return ZF2x\Mail\Message
     */
    public function baseMessageToZF2xMessage(ZendMailMessage $message)
    {
        throw new Exception("not yet implemented");
    }

    /**
     * Gets the Default From Address.
     *
     * @return Zend\Mail\AddressList
     */
    public function getDefaultFromAddress()
    {
        return $this->defaultFromAddress;
    }

    /**
     * Sets the Default From Address.
     *
     * @param Zend\Mail\Address $defaultFromAddress the default from address
     *
     * @return self
     */
    public function setDefaultFromAddress(AddressList $defaultFromAddress)
    {
        $this->defaultFromAddress = $defaultFromAddress;

        return $this;
    }

    /**
     * Gets the Default Renderer for View Model Rendering.
     *
     * @return Zend\View\Renderer\RendererInterface
     */
    public function getDefaultRenderer()
    {
        return $this->defaultRenderer;
    }

    /**
     * Sets the Default Renderer for View Model Rendering.
     *
     * @param Zend\View\Renderer\RendererInterface $defaultRenderer the default renderer
     *
     * @return self
     */
    public function setDefaultRenderer(RendererInterface $defaultRenderer)
    {
        $this->defaultRenderer = $defaultRenderer;

        return $this;
    }

    /**
     * Setup Basic Renderer
     *
     * renderer with template path stack resolver and php renderer
     *
     * @param  array  $templatePaths
     *
     * @return self
     */
    public function setupBasicRenderer(array $templatePaths)
    {
        $resolver = new TemplatePathStack;
        $resolver->setPaths($templatePaths);
        $renderer = new PhpRenderer;
        $renderer->setResolver($resolver);
        $this->setDefaultRenderer($renderer);

        return $this;
    }

    /**
     * Gets the Default Transport.
     *
     * @return TransportInterface
     */
    public function getDefaultTransport()
    {
        return $this->defaultTransport;
    }

    /**
     * Sets the Default Transport.
     *
     * @param TransportInterface $defaultTransport the default transport
     *
     * @return self
     */
    public function setDefaultTransport(TransportInterface $defaultTransport)
    {
        $this->defaultTransport = $defaultTransport;

        return $this;
    }
}
