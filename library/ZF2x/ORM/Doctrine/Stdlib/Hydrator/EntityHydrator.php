<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\ORM\Doctrine\Stdlib\Hydrator;

use Datetime;
use Doctrine\ORM\Proxy\Proxy;
use Doctrine\Orm\EntityManager;
use Zend\Stdlib\Hydrator\AbstractHydrator;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use ZF2x\ORM\Doctrine\DoctrineORMAwareInterface;

/**
 * Doctrine Entity Hydrator
 */
class EntityHydrator extends AbstractHydrator implements DoctrineORMAwareInterface
{
    /**
     * Entity Manager
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * Extract entities and return id of proxyies
     * @param  [type] $object [description]
     * @return [type]         [description]
     */
    public function extract($object, $splHashes = array())
    {
        //check for cyclical dependencies
        $hash = spl_object_hash($object);
        if (in_array($hash, $splHashes)) {
            return array();
        }
        $splHashes[] = $hash;

        $metadata = $this->getEntityManager()->getClassMetadata(get_class($object));
        $mappings = $metadata->getAssociationMappings();
        $data = array();
        $fields = array_merge($metadata->getFieldNames(), $metadata->getAssociationNames());
        foreach ($fields as $field) {

            $getter = 'get'.$field;
            if (method_exists($object, $getter)) {
                $value = $this->extractValue($field, $object->$getter(), $object);
            }

            if ($metadata->isCollectionValuedAssociation($field)) {
                //\Doctrine\Common\Util\Debug::dump($value);


            } elseif ($metadata->isSingleValuedAssociation($field)) {
                if (!is_object($value)) {
                    continue;
                }

                //if the value object does not have a relation back to this object,
                //return values not just id.

                $class = get_class($value);
                $metadata = $this->getEntityManager()->getClassMetadata($class);
                $biDirectional = false;
                foreach ($metadata->getAssociationNames() as $sfield) {
                    if ($metadata->getAssociationTargetClass($sfield) === get_class($object)) {
                        $biDirectional = true;
                    }
                }

                if ($biDirectional) {
                    //Bi Directional
                    $id = $metadata->getIdentifier();
                    $id = $id[0];
                    $getter = 'get'.$id;
                    $value = $this->extractValue($id, $value->$getter(), $value);
                } else {
                    //Uni Directional
                    $value = $this->extract($value, $splHashes);
                }

                /*if (!is_object($value)) {
                    continue;
                } elseif ($value instanceof Proxy) {
                    $value = $this->extract($value, $splHashes);
                } else {
                    $value = $this->extract($value, $splHashes);
                }*/
            }
            if ($value instanceof Collection) {
                $r = array();
                foreach ($value as $entity) {
                    $class = get_class($entity);
                    $metadata = $this->getEntityManager()->getClassMetadata($class);
                    $id = $metadata->getIdentifier();
                    $id = $id[0];
                    $getter = 'get'.$id;
                    $r[] = $this->extractValue($id, $entity->$getter(), $entity);
                }
                $value = $r;
            }
            $data[$field] = $value;

        }
        //var_dump(get_class($object));
        //\Doctrine\Common\Util\Debug::dump($object);

        return $data;
    }

    /**
     * [hydrate description]
     * @param  array  $data   [description]
     * @param  [type] $object [description]
     * @return [type]         [description]
     */
    public function hydrate(array $data, $object)
    {
        //need to add ability to hydrate object given id from database
        //check for null allowed
        //convert certain types (date)
        $metadata = $this->getEntityManager()->getClassMetadata(get_class($object));
        foreach ($data as $field => $value) {

            $value = $this->handleTypeConversions($value, $metadata->getTypeOfField($field));

            $setter = 'set'.$field;
            $getter = 'get'.$field;
            //if (!method_exists(get_class($object), $setter)) {
            //    continue;
            //}

            if ($metadata->isIdentifier($field)) {
                $result = $this->findObject(get_class($object), $field, $value);
                if ($result) {
                    $object = $result;
                }
            }

            if ($metadata->isSingleValuedAssociation($field)) {
                $target = $metadata->getAssociationTargetClass($field);

                if (!is_object($value) && !is_array($value)) {
                    $metadata = $this->getEntityManager()->getClassMetadata($target);
                    $id = $metadata->getIdentifier();
                    $id = $id[0];
                    $result = $this->findObject($target, $id, $value);
                    if ($result) {
                        $value = $result;
                    } else {
                        $object->{$setter}(null);
                    }
                }

                if ($value instanceof $target) {
                    //check if bi directional or unidirectional.
                    //if unidirectional, set $value on $object
                    //if bidirection, set $value on $object and $object on $value
                    $class = get_class($value);
                    $smetadata = $this->getEntityManager()->getClassMetadata(get_class($value));
                    $biDirectional = false;
                    foreach ($smetadata->getAssociationNames() as $sfield) {
                        if ($smetadata->getAssociationTargetClass($sfield) === get_class($object)) {
                            $biDirectional = true;
                            $biDirectionalGetter = 'get'.$sfield;
                            $biDirectionalSetter = 'set'.$sfield;
                        }
                    }
                    $object->{$setter}($value);
                    if ($biDirectional) {
                        if ($smetadata->isCollectionValuedAssociation($sfield)) {
                            //$value->{$biDirectionalGetter}->add($object);
                        } else {
                            $value->{$biDirectionalSetter}($object);
                        }
                    }
                }

                /*if ($value instanceof $target) {
                    $object->{$setter}($value);

                    //bidirectional
                    if (get_class($object) == $target) {
                        $value->{$setter}($object);
                    }
                }*/

            } elseif ($metadata->isCollectionValuedAssociation($field)) {
                $collection = $object->{$getter}();
                if (!is_object($collection) || ! $collection instanceof Collection) {
                    $collection = new ArrayCollection;
                    $object->{$setter}($collection);
                }
                $collection->clear();

                if (null === $value || !is_array($value)) {
                    continue;
                }




                //if (!is_array($value)&&!is_object($value)) {
                //    continue;
                //}
                $targetClass = $metadata->getAssociationTargetClass($field);
                $childMeta = $this->getEntityManager()->getClassMetadata($targetClass);
                foreach ($childMeta->getAssociationMappings() as $name => $assoc) {
                    if ($assoc['targetEntity'] === get_class($object) && null !== $assoc['inversedBy']) {
                        $set = 'set'.$name;
                    }
                }
                if (is_object($value) && $value instanceof $targetClass) {
                    $object->{$getter}()->add($value);
                }
                foreach ($value as $many) {
                    if (! $many instanceof $targetClass) {
                        if (is_string($many)) {
                            $result = $this->findObject($targetClass, 'id', $many);
                            if ($result) {
                                $many = $result;
                            }
                        } else {
                            $many = $this->hydrate($many, new $targetClass);
                        }
                    }
                    $object->{$getter}()->add($many);
                    //if (method_exists($many, $set)) {
                    //    $many->{$set}($object);
                    //}
                }

            } else {
                if (method_exists($object, $setter)) {
                    $object->{$setter}($value);
                }
            }
        }
        /*if (get_class($object) == 'AgencyPal\Entity\AgencyAccount') {
            //var_dump($data);
            \Doctrine\Common\Util\Debug::dump($data);
            \Doctrine\Common\Util\Debug::dump($object);
            exit();
        }
        */
        return $object;
    }

    protected function handleTypeConversions($value, $typeOfField)
    {
        switch($typeOfField) {
            case 'datetimetz':
            case 'datetime':
            case 'time':
            case 'date':
                if ('' === $value) {
                    return null;
                }
                if (is_numeric($value)) {
                    $dateTime = new DateTime();
                    $dateTime->setTimestamp($value);
                    $value = $dateTime;
                } elseif (is_string($value)) {
                    $value = new DateTime($value);
                }
                break;
            default:
        }

        return $value;
    }


    public function findObject($objectClass, $field, $value)
    {
        $result = $this->entityManager->getRepository($objectClass)->findOneBy(array($field=>$value));
        return $result;
    }

    /**
     * Gets the Entity Manager.
     *
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * Sets the Entity Manager.
     *
     * @param EntityManager $entityManager the entityManager
     *
     * @return self
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;

        return $this;
    }
}
