<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Identification\Event;

use Zend\EventManager\Event;
use Zend\Authentication\Adapter\AdapterInteface;
use ZF2x\Identification\Identity\IdentityInterface;

/**
 * Identity Event for Identification
 */
class IdentityEvent extends Event implements IdentityEventInterface
{
    const IDENTIFY_EVENT    = 'identify.identity';

    const UPDATE_EVENT      = 'update.identity';

    const CLEAR_EVENT       = 'clear.identity';

    /**
     * @var AdapterInterface
     */
    protected $adapter;

    /**
     * @var AuthIdentityInterface
     */
    protected $identity;

    /**
     * Gets the value of adapter.
     *
     * @return IdentityAdapterInterface
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * Sets the value of adapter.
     *
     * @param AdapterInterface $adapter the adapter
     *
     * @return self
     */
    public function setAdapter(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;

        return $this;
    }

    /**
     * Gets the value of identity.
     *
     * @return AuthIdentityInterface
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * Sets the value of identity.
     *
     * @param AuthIdentityInterface $identity the identity
     *
     * @return self
     */
    public function setIdentity(IdentityInterface $identity)
    {
        $this->identity = $identity;

        return $this;
    }
}
