<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Form\Element;

use DateTime as PhpDateTime;
use Exception;
use Zend\Form\FormInterface;
use Zend\Form\Exception\InvalidArgumentException;
use Zend\Validator\ValidatorInterface;
use Zend\Validator\Date as DateValidator;
use Zend\Form\Element\DateSelect;
use Zend\Form\Element\Select;
use Zend\Form\Element;
use Zend\Form\ElementPrepareAwareInterface;
use Zend\InputFilter\InputProviderInterface;

class TimeEntrySelect extends Element implements InputProviderInterface, ElementPrepareAwareInterface
{
    protected $validator;

    /**
     * Select form element that contains values for hour
     *
     * @var Select
     */
    protected $hourElement;

    /**
     * Select form element that contains values for minute
     *
     * @var Select
     */
    protected $minuteElement;

    /**
     * Constructor. Add the hour, minute and second select elements
     *
     * @param  null|int|string  $name    Optional name for the element
     * @param  array            $options Optional options for the element
     */
    public function __construct($name = null, $options = array())
    {
        $this->hourElement   = new Select('hour');
        $this->minuteElement = new Select('minute');
        parent::__construct($name, $options);
    }

    public function setOptions($options)
    {
        parent::setOptions($options);
        return $this;
    }

    /**
     * @return Select
     */
    public function getHourElement()
    {
        return $this->hourElement;
    }

    /**
     * @return Select
     */
    public function getMinuteElement()
    {
        return $this->minuteElement;
    }

    /**
     * Set the hour attributes
     *
     * @param  array $hourAttributes
     * @return DateSelect
     */
    public function setHourAttributes(array $hourAttributes)
    {
        $this->hourElement->setAttributes($hourAttributes);
        return $this;
    }

    /**
     * Get the hour attributes
     *
     * @return array
     */
    public function getHourAttributes()
    {
        return $this->hourElement->getAttributes();
    }

    /**
     * Set the minute attributes
     *
     * @param  array $minuteAttributes
     * @return DateSelect
     */
    public function setMinuteAttributes(array $minuteAttributes)
    {
        $this->minuteElement->setAttributes($minuteAttributes);
        return $this;
    }

    /**
     * Get the minute attributes
     *
     * @return array
     */
    public function getMinuteAttributes()
    {
        return $this->minuteElement->getAttributes();
    }

    /**
     * @param mixed $value
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @return void|\Zend\Form\Element
     */
    public function setValue($value)
    {
        if (!$value) {
            $value['hour'] = 0;
            $value['minute'] = 0;
        }

        if (is_scalar($value)) {
            $v = explode('.', (string) $value);
            $value = array();
            $value['hour'] = $v[0];
            $value['minute'] = $v[1];
        }

        $this->hourElement->setValue($value['hour']);
        $this->minuteElement->setValue($value['minute']);
    }

    /**
     * Prepare the form element (mostly used for rendering purposes)
     *
     * @param  FormInterface $form
     * @return mixed
     */
    public function prepareElement(FormInterface $form)
    {
        $name = $this->getName();
        $this->hourElement->setName($name . '[hour]');
        $this->minuteElement->setName($name . '[minute]');
    }

    /**
     * Get validator
     *
     * @return ValidatorInterface
     */
    protected function getValidator()
    {
        if (null === $this->validator) {
            $this->validator = new \Zend\Validator\Callback(
                array(
                    'callback' => function ($value) {
                        return true;
                    }
                )
            );
        }

        return $this->validator;
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInput()}.
     *
     * @return array
     */
    public function getInputSpecification()
    {
        return array(
            'name' => $this->getName(),
            'required' => false,
            'filters' => array(
                array(
                    'name'    => 'Callback',
                    'options' => array(
                        'callback' => function ($v) {
                            $i = 0.0;
                            // Convert the date to a specific format
                            if (is_array($v)) {
                                $s = $v['hour'].'.'.$v['minute'];
                                $i = floatval($s);
                            }
                            return $i;
                        }
                    )
                )
            ),
            'validators' => array(
                $this->getValidator(),
            )
        );
    }

    /**
     * Clone the element (this is needed by Collection element, as it needs different copies of the elements)
     */
    public function __clone()
    {
        $this->hourElement   = clone $this->monthElement;
        $this->minuteElement = clone $this->minuteElement;
    }
}
