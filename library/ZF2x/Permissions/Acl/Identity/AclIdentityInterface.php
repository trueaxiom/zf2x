<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Identity;

use ZF2x\Identification\Identity\IdentityInterface;

interface AclIdentityInterface extends IdentityInterface
{
    public function getRole();
    public function setRole($role);
}
