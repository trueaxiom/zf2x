<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\View\Helper\BootstrapForm;

class Module
{
    public function getConfig()
    {
        return array(
            'view_helpers' => array(
                'invokables' => array(
                    'bootstrapForm' => 'ZF2x\View\Helper\BootstrapForm\Form',
                    'bootstrapRow' => 'ZF2x\View\Helper\BootstrapForm\Row',
                    'bootstrapCollection' => 'ZF2x\View\Helper\BootstrapForm\Collection'

                ),
            ),
        );
    }
}
