<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Module\Redirect;

use Zend\Stdlib\AbstractOptions;

/**
 * Options class for Acl Factory
 */
class Options extends AbstractOptions
{
    /**
     * Redirect Configuration
     *
     * @var array
     */
    protected $redirect = array();

    /**
     * Gets the Redirect Configuration.
     *
     * @return array
     */
    public function getRedirect()
    {
        return $this->redirect;
    }

    /**
     * Sets the Redirect Configuration.
     *
     * @param array $redirect the redirect
     *
     * @return self
     */
    public function setRedirect(array $redirect)
    {
        $this->redirect = $redirect;

        return $this;
    }
}
