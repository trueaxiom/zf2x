<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\ORM\Doctrine\Stdlib\Hydrator;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Entity Hydrator Factory
 */
class EntityHydratorFactory implements FactoryInterface
{
    /**
     * Create a Doctrine ORM Entity Hydrator
     * @param  ServiceLocatorInterface $serviceLocator Service Locator
     * @return AuthProvider Doctrine ORM Authentication Provider
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocator = $serviceLocator->getServiceLocator();
        $entityManager = $serviceLocator->get('DoctrineEntityManager');
        $hydrator = new EntityHydrator;
        return $hydrator;
    }
}
