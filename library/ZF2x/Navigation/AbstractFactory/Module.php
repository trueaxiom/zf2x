<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Navigation\AbstractFactory;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * @author Richard Jennings <rich@trueaxiom.co.uk>
 */
class Module implements ConfigProviderInterface
{

    /**
     * Get Config
     *
     * @return array
     */
    public function getConfig()
    {
        return array(
            'service_manager' => array(
                'abstract_factories' => array(
                    'ZF2x\Navigation\AbstractFactory\AbstractNavigationFactory'
                ),
            )
        );
    }
}
