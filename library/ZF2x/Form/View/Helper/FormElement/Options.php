<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Form\View\Helper\FormElement;

use Zend\Stdlib\AbstractOptions;

/**
 * Form Element Helper Mapping Options
 */
class Options extends AbstractOptions
{
    protected $mappings = array();

    /**
     * Gets the value of mappings.
     *
     * @return mixed
     */
    public function getMappings()
    {
        return $this->mappings;
    }

    /**
     * Sets the value of mappings.
     *
     * @param mixed $mappings the mappings
     *
     * @return self
     */
    public function setMappings($mappings)
    {
        $this->mappings = $mappings;

        return $this;
    }
}
