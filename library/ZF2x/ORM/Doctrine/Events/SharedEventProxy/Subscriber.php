<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\ORM\Doctrine\Events\SharedEventProxy;

use Doctrine\ORM\Events;
use Doctrine\ORM\Proxy\Proxy;
use Doctrine\Common\EventArgs;
use Zend\EventManager\EventManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnClearEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManagerAwareInterface;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class Subscriber implements EventManagerAwareInterface, EventSubscriber
{
    /**
     * Event Manager
     * @var EventManagerInterface
     */
    protected $eventManager;

    /**
     * Events to subscribe to
     * @var array
     */
    protected $subscribedEvents = array(
        'preRemove',
        'postRemove',
        'prePersist',
        'postPersist',
        'preUpdate',
        'postUpdate',
        'postLoad',
        'loadClassMetadata',
        'preFlush',
        'onFlush',
        'postFlush',
        'onClear'
    );

    /**
     * preRemove Event
     * @param  LifecycleEventArgs $args Doctrine Event Arguments
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $this->setIdentifiers($args);
        $this->getEventManager()->trigger("preRemoveEntity", $this, $args);
    }

    /**
     * postRemove Event
     * @param  LifecycleEventArgs $args Doctrine Event Arguments
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $this->setIdentifiers($args);
        $this->getEventManager()->trigger("postRemoveEntity", $this, $args);
    }

    /**
     * prePersist Event
     * @param  LifecycleEventArgs $args Doctrine Event Arguments
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->setIdentifiers($args);
        $this->getEventManager()->trigger("prePersistEntity", $this, $args);
    }

    /**
     * postPersist Event
     * @param  LifecycleEventArgs $args Doctrine Event Arguments
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->setIdentifiers($args);
        $this->getEventManager()->trigger("postPersistEntity", $this, $args);
    }

    /**
     * preUpdate Event
     * @param  preUpdateEventArgs $args Doctrine Event Arguments
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $this->setIdentifiers($args);
        $this->getEventManager()->trigger("preUpdateEntity", $this, $args);
    }

    /**
     * postUpdate Event
     * @param  LifecycleEventArgs $args Doctrine Event Arguments
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->setIdentifiers($args);
        $this->getEventManager()->trigger("postUpdateEntity", $this, $args);
    }

    /**
     * postLoad Event
     * @param  LifecycleEventArgs $args Doctrine Event Arguments
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $this->setIdentifiers($args);
        $this->getEventManager()->trigger("postLoadEntity", $this, $args);
    }

    /**
     * loadClassMetadata Event
     * @param  LoadClassMetadataEventArgs $args Doctrine Event Arguments
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $args)
    {
        $eventManager = $this->getEventManager();
        $eventManager->trigger("loadClassMetadata", $this, $args);
    }

    /**
     * preFlush Event
     * @param  PreFlushEventArgs $args Doctrine Event Arguments
     */
    public function preFlush(PreFlushEventArgs $args)
    {
        $eventManager = $this->getEventManager();
        $eventManager->trigger("preFlush", $this, $args);
    }

    /**
     * onFlush Event
     * @param  OnFlushEventArgs $args Doctrine Event Arguments
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $eventManager = $this->getEventManager();
        $eventManager->trigger("onFlush", $this, $args);
        //more specific events
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        foreach ($uow->getScheduledEntityInsertions() AS $entity) {
            $eventManager->setIdentifiers(array(get_class($entity)));
            $eventManager->trigger("onFlushInsertions", $this, $args);
        }
        foreach ($uow->getScheduledEntityUpdates() AS $entity) {
            $eventManager->setIdentifiers(array(get_class($entity)));
            $eventManager->trigger("onFlushUpdates", $this, $args);
        }
        /*
        foreach ($uow->getScheduledEntityDeletions() AS $entity) {
        }
        foreach ($uow->getScheduledCollectionDeletions() AS $col) {
        }
        foreach ($uow->getScheduledCollectionUpdates() AS $col) {
        }
        */
    }

    /**
     * postFlush Event
     * @param  PostFlushEventArgs $args Doctrine Event Arguments
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        $eventManager = $this->getEventManager();
        $eventManager->trigger("postFlush", $this, $args);
    }

    /**
     * onClear Event
     * @param  OnClearEventArgs $args Doctrine Event Arguments
     */
    public function onClear(OnClearEventArgs $args)
    {
        $eventManager = $this->getEventManager();
        $eventManager->trigger("onClear", $this, $args);
    }

    /**
     * Gets the Event Manager.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->eventManager) {
            $eventManager = new EventManager;
            $this->setEventManager($eventManager);
        }
        return $this->eventManager;
    }

    /**
     * Sets the Event Manager.
     *
     * @param EventManagerInterface $eventManager the event manager
     *
     * @return self
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->eventManager = $eventManager;

        return $this;
    }

    /**
     * Gets the Events to subscribe to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return $this->subscribedEvents;
    }

    /**
     * Sets the Events to subscribe to.
     *
     * @param array $subscribedEvents the subscribed events
     *
     * @return self
     */
    public function setSubscribedEvents(array $subscribedEvents)
    {
        $this->subscribedEvents = $subscribedEvents;

        return $this;
    }

    /**
     * Sets up identifier such that shared manager Entity Class matches Identifier for Lifecycle Events
     * @param [type] $args [description]
     */
    protected function setIdentifiers($args)
    {
        if ($args instanceof LifecycleEventArgs) {
            $entity = $args->getEntity();
            if ($entity instanceof Proxy) {
                $class = get_parent_class($entity);
            } else {
                $class = get_class($entity);
            }
            $this->getEventManager()->setIdentifiers(array($class));
        }
    }
}
