<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Module\RoleEntityConfigMapper\Mapper;

use Zend\Stdlib\Hydrator\ClassMethods;
use ZF2x\Permissions\Acl\Entity\RoleEntity;

class RoleEntityConfigMapper
{
    protected $authRoles = array();

    public function getAuthRoles()
    {
        return $this->authRoles;
    }

    public function setAuthRoles($config)
    {
        $this->authRoles = $config;
    }

    public function findOneByAuthId($id)
    {
        $result = array();
        $authRoles = $this->getAuthRoles();
        foreach ($authRoles as $i => $row) {
            if (isset($row['auth_id'])&&$row['auth_id']==$id) {
                return $this->hydrateEntity($row);
            }
        }
    }

    public function hydrateEntity($array)
    {
        $hydrator = new ClassMethods;
        $authEntity = new RoleEntity;
        $hydrator->hydrate($array, $authEntity);
        return $authEntity;
    }
}
