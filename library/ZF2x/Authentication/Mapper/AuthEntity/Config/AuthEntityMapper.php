<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Authentication\Mapper\AuthEntity\Config;

use Zend\Stdlib\Hydrator\ClassMethods;
use ZF2x\Authentication\Entity\AuthEntity;
use ZF2x\Authentication\Mapper\AuthEntity\AuthEntityMapperInterface;

class AuthEntityMapper implements AuthEntityMapperInterface
{
    /**
     * Identities
     * @var array
     */
    protected $identities = array();

    public function findOneByAuthId($id)
    {
        $identities = $this->getIdentities();
        foreach ($identities as $row) {
            if (isset($row['auth_id']) && $row['auth_id'] == $id) {
                $identity = $this->hydrateEntity($row);
                return $identity;
            }
        }
    }

    public function findOneByIdentity($identity)
    {
        $identities = $this->getIdentities();
        foreach ($identities as $row) {
            if (isset($row['identity']) && $row['identity'] === $identity) {
                $identity = $this->hydrateEntity($row);
                return $identity;
            }
        }
    }

    public function hydrateEntity($array)
    {
        $hydrator = new ClassMethods;
        $authEntity = new AuthEntity;
        $hydrator->hydrate($array, $authEntity);
        return $authEntity;
    }

    /**
     * Gets the value of identities.
     *
     * @return mixed
     */
    public function getIdentities()
    {
        return $this->identities;
    }

    /**
     * Sets the value of identities.
     *
     * @param mixed $identities the identities
     *
     * @return self
     */
    public function setIdentities($identities)
    {
        $this->identities = $identities;

        return $this;
    }
}
