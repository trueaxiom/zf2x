<?php

//change directory to project root
//from Doctrine Module
$previousDir = null;
while (!file_exists('config/application.config.php')) {
    $dir = dirname(getcwd());

    if ($previousDir === $dir) {
        throw new RuntimeException(
            'Unable to locate "config/application.config.php": ' .
            'is DoctrineModule in a subdir of your application skeleton?'
        );
    }

    $previousDir = $dir;
    chdir($dir);
}
//

require 'vendor/autoload.php';

use Zend\Mvc\Application;
use Symfony\Component\Console\Helper;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Symfony\Component\Console\Helper\HelperSet;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;

$app = Application::init(require 'config/application.config.php');
$em = $app->getServiceManager()->get('DoctrineEntityManager');
$helperSet = new HelperSet(
    array(
        'db' => new ConnectionHelper($em->getConnection()),
        'em' => new EntityManagerHelper($em)
    )
);
ConsoleRunner::run($helperSet);
