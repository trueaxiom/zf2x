<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Identification\Manager;

use Zend\Session\Container;
use Zend\Stdlib\HydratorInterface;
use Zend\EventManager\EventInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use ZF2x\Identification\Event\IdentityEvent;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use ZF2x\Identification\Identity\IdentityInterface;

abstract class AbstractIdentityManager implements ListenerAggregateInterface
{
    const IDENTITY_CLASS = '';
    const SESSION_NAMESPACE = '';

    /**
     * @var Container|null
     */
    protected $container;

    /**
     * @var EventManagerInterface|null
     */
    protected $eventManager;

    /**
     * @var IdentityInterface|null
     */
    protected $identity;

    /**
     * Get Identities From Auth Identity
     * @var null
     */
    protected $mapper = null;

    /**
     * The Hydrator
     * @var Hydrator Interface
     */
    protected $hydrator = null;

    /**
     * Attach Events
     * @param  EventManagerInterface $events [description]
     * @return [type]                        [description]
     */
    public function attach(EventManagerInterface $events)
    {
        die(__METHOD__);
    }

    /**
     * Detach Events
     * @param  EventManagerInterface $events [description]
     * @return [type]                        [description]
     */
    public function detach(EventManagerInterface $events)
    {
        die(__METHOD__);
    }

    /**
     * Get Container
     *
     * @return Container
     */
    public function getContainer()
    {
        if (null === $this->container) {
            $this->container = new Container(static::SESSION_NAMESPACE);
        }
        return $this->container;
    }

    /**
     * Set Container
     *
     * @param Container $container
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Get Event Manager
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        return $this->eventManager;
    }

    /**
     * Set Event Manager
     *
     * @param EventManagerAwareInterface $eventManager
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->eventManager = $eventManager;
    }

    /**
     * Get Hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        if ($this->hydrator === null) {
            return new ClassMethods;
        } else {
            return $this->hydrator;
        }
    }

    /**
     * Set Hydrator
     * @param HydratorInterface $hydrator The Hydrator
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Get Identity Data
     *
     * @return array Identity Data
     */
    public function getIdentityData()
    {
        $data = $this->getContainer()->getManager()->getStorage()->toArray();
        return isset($data[static::SESSION_NAMESPACE]) ? $data[static::SESSION_NAMESPACE] : array();
    }

    /**
     * Has Identity
     *
     * @return boolean has identity
     */
    public function hasIdentity()
    {
        return count($this->getIdentityData()) > 0 ? true : false;
    }

    /**
     * Create a new Identity
     *
     * @param  array  $data Identity Data
     * @return IdentityInterface Identity
     */
    public function createIdentity(array $data)
    {
        $identityClass = static::IDENTITY_CLASS;
        $hydrator = new ClassMethods;
        $identity = $hydrator->hydrate($data, new $identityClass);
        $this->setIdentity($identity);
        return $identity;
    }

    /**
     * Update Identity
     *
     * @param  array  $data data to update
     * @return IdentityInterface Identity
     */
    public function updateIdentity($authId)
    {
        $mapper = $this->getMapper();
        $entity = $mapper->findOneByAuthId($authId);
        if (!$entity) {
            return false;
        }
        $hydrator = $this->getHydrator();
        $data = $hydrator->extract($entity);
        $identityClass = static::IDENTITY_CLASS;
        $identity = $hydrator->hydrate($data, new $identityClass);
        $this->setIdentity($identity);
        $this->identity = $identity;
    }

    /**
     * Get Identity
     *
     * @return IdentityInterface Identity
     */
    public function getIdentity()
    {
        if (!$this->container instanceof Container) {
            $data = $this->getContainer()->getManager()->getStorage()->toArray();
            if (empty($data)) {
                return null;
            }
            $identityClass = static::IDENTITY_CLASS;
            $hydrator = new ClassMethods;
            $this->identity = $hydrator->hydrate($this->getIdentityData(), new $identityClass);
        }
        return $this->identity;
    }

    /**
     * Set Identity
     *
     * @param IdentityInterface $identity Identity
     */
    public function setIdentity(IdentityInterface $identity = null)
    {
        if ($identity === null) {
            return $this->clearIdentity();
        }
        $hydrator = new ClassMethods;
        $this->getContainer()->exchangeArray($hydrator->extract($identity));
        return $identity;
    }

    /**
     * Clear Identity
     *
     * @return null
     */
    public function clearIdentity()
    {
        $this->identity = null;
        $this->getContainer()->getManager()->getStorage()->clear(static::SESSION_NAMESPACE);
    }

    /**
     * Get Identity Property from Identity
     *
     * @param  string  $property  property name
     * @param  string  $delimiter delimiter for path (redundant?)
     * @param  boolean $default   default if value does not exist
     * @return mixed              Identity Property
     */
    public function getIdentityProperty($property, $default = false)
    {
        return $this->getContainer()->offsetExists($property)
            ? $this->getContainer()->offsetGet($property)
            : $default;
    }

    /**
     * Get all Identity Properties for this namespace as Array
     *
     * @return array Identity properties
     */
    public function getIdentityProperties()
    {
        return ArrayUtils::getPath($this->getStorage()->toArray(), self::STORAGE_KEY);
    }

    /**
     * Gets the Get Identities From Auth Identity.
     *
     * @return null
     */
    public function getMapper()
    {
        return $this->mapper;
    }

    /**
     * Sets the Get Identities From Auth Identity.
     *
     * @param null $mapper the mapper
     *
     * @return self
     */
    public function setMapper($mapper)
    {
        $this->mapper = $mapper;

        return $this;
    }

    /**
     * On Update Identity Event
     *
     * @param  IdentityEvent $event [description]
     * @return [type]               [description]
     */
    public function onUpdate(IdentityEvent $event)
    {
        $auth = $event->getIdentity();
        $authId = $auth->getAuthId();
        $this->updateIdentity($authId);
    }

    /**
     * On Identify Event
     *
     * @param  IdentityEvent $event Identify Event
     * @return IdentityInterface|null
     */
    public function onIdentify(EventInterface $event)
    {
        return $this->getIdentity();
    }

    /**
     * On Clear Identity Event
     *
     * @param  EventInterface $event Event
     * @return null
     */
    public function onClear(EventInterface $event)
    {
        return $this->clearIdentity();
    }
}
