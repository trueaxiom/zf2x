<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\ORM\Doctrine;

use Doctrine\ORM\EntityManager;

/**
 * Doctrine ORM Aware Interface
 */
interface DoctrineORMAwareInterface
{
    /**
     * Set Entity Manager
     * @param EntityManager $entityManager Entity Manager
     */
    public function setEntityManager(EntityManager $entityManager);

    /**
     * Get Entity Manager
     * @return EntityManager Entity Manager
     */
    public function getEntityManager();
}
