<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Mail;

use Zend\Mail\Address;
use Zend\Mail\AddressList;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $service = new Service;
        $options = $serviceLocator->get('zf2x.mail.options');
        $rendererAlias = $options->getDefaultRenderer();
        if ($rendererAlias) {
            $renderer = $serviceLocator->get($rendererAlias);
            $service->setDefaultRenderer($renderer);
        }
        $transportAlias = $options->getDefaultTransport();
        if ($transportAlias) {
            $transport = $serviceLocator->get($transportAlias);
            $service->setDefaultTransport($transport);
        }
        $fromAddress = $options->getDefaultFromEmailAddress();
        if ($fromAddress) {
            $fromName = $options->getDefaultFromName();
            $from = new AddressList;
            $from->add(new Address($fromAddress, $fromName));
            $service->setDefaultFromAddress($from);
        }
        return $service;
    }
}
