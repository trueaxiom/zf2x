<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Mandrill\Mail\Transport;

use ZF2x\Mail\Message;
use Mandrill_Messages;
use ZF2x\Mandrill\Service;
use ZF2x\Mail\Transport\TransportInterface;

class MandrillAPI implements TransportInterface
{
    /**
     * MandrillService
     *
     * @var ZF2x\Mandrill\Service
     */
    protected $service;

    /**
     * Enable Asyncronous Sending
     *
     * @var boolean
     */
    protected $async = false;

    /**
     * Name of dedicated IP Pool if any
     *
     * @var string
     */
    protected $ipPool;

    /**
     * Queue to send at UTC timestamp
     *
     * @var integer
     */
    protected $sendAt;

    /**
     * Send Message
     *
     * @param  Message $message
     *
     * @return array
     */
    public function send(\Zend\Mail\Message $message)
    {
        $service = $this->getService();
        return $service->sendMessage($message, $this->getAsync(), $this->getIpPool(), $this->getSendAt());
    }

    /**
     * Gets the MandrillService.
     *
     * @return ZF2x\Mandrill\Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Sets the MandrillService.
     *
     * @param ZF2x\Mandrill\Service $service the service
     *
     * @return self
     */
    public function setService(Service $service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Gets the Enable Asyncronous Sending.
     *
     * @return boolean
     */
    public function getAsync()
    {
        return $this->async;
    }

    /**
     * Sets the Enable Asyncronous Sending.
     *
     * @param boolean $async the async
     *
     * @return self
     */
    public function setAsync($async)
    {
        $this->async = $async;

        return $this;
    }

    /**
     * Gets the Name of dedicated IP Pool if any.
     *
     * @return string
     */
    public function getIpPool()
    {
        return $this->ipPool;
    }

    /**
     * Sets the Name of dedicated IP Pool if any.
     *
     * @param string $ipPool the ip pool
     *
     * @return self
     */
    public function setIpPool($ipPool)
    {
        $this->ipPool = $ipPool;

        return $this;
    }

    /**
     * Gets the Queue to send at UTC timestamp.
     *
     * @return integer
     */
    public function getSendAt()
    {
        return $this->sendAt;
    }

    /**
     * Sets the Queue to send at UTC timestamp.
     *
     * @param integer $sendAt the send at
     *
     * @return self
     */
    public function setSendAt($sendAt)
    {
        $this->sendAt = $sendAt;

        return $this;
    }
}
