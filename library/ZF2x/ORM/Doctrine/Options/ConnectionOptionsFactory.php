<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\ORM\Doctrine\Options;

use ZF2x\Stdlib\AbstractOptionsFactory;

/**
 * Connection Options Factory
 */
class ConnectionOptionsFactory extends AbstractOptionsFactory
{
    /**
     * Path from which to retrieve configuration
     *
     * @const string Configuration Path
     */
    const CONFIG_PATH = 'doctrine.orm.connection';

    /**
     * The Options Class that this factory should instantiate and populate
     */
    const OPTIONS_CLASS = 'ZF2x\ORM\Doctrine\Options\ConnectionOptions';
}
