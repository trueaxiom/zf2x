<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Authentication\Mapper\AuthEntity\Config;

class Module
{
    public function getConfig()
    {
        return array(
            'service_manager' => array(
                'factories' => array(
                    'AuthEntityMapper' => function ($sl) {
                        $mapper         = new AuthEntityMapper;
                        $options        = $sl->get('AuthEntityConfigMapperOptions');
                        $identities     = $options->getIdentities();
                        $mapper->setIdentities($identities);
                        return $mapper;
                    },
                    'AuthEntityConfigMapperOptions' => 'ZF2x\Authentication\Mapper\AuthEntity\Config\OptionsFactory',
                ),
            )
        );
    }
}
