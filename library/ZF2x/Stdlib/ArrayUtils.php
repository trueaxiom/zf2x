<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Stdlib;

use Zend\Stdlib\ArrayUtils as ZendArrayUtils;

/**
 * Array Utililties additional to those provided in Zend\Stdlib\ArrayUtils
 */
class ArrayUtils extends ZendArrayUtils
{
    /**
     * Return value of key specified by traversing path of array keys or default
     *
     * Take an array, a path, the delimiter for the path expresion and return the
     * value of the array found by traversing the path.
     *
     * 'success' == self::getPath(
     *     array('t'=>array('e'=>array('s'=>array('t' => 'success')))),
     *     't.e.s.t'
     *  );
     *  false == self::getPath(
     *     array('t'=>array('e'=>array('s'=>array('t' => 'success')))),
     *     't.e.s.t.2',
     *     '.',
     *     false
     *  );
     *
     * @param  array   $array
     * @param  string  $path
     * @param  boolean $default
     * @param  string  $delimiter
     * @return mixed
     */
    public static function getPath(array $array, $path, $delimiter = '.', $default = false)
    {
        if (null!==$delimiter) {
            $parts = explode($delimiter, $path);
        } else {
            $parts = array($path);
        }
        if (is_array($parts) && isset($parts[0]) && isset($array[$parts[0]])) {
            if (!isset($parts[1])) {
                return $array[$parts[0]];
            } else {
                $array = $array[$parts[0]];
                array_shift($parts);
                return self::getPath(
                    $array,
                    implode($delimiter, $parts),
                    $delimiter,
                    $default
                );
            }
        }
        return $default;
    }
}
