<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Listener;

use Zend\EventManager\EventInterface;
use Zend\Permissions\Acl\Resource\GenericResource;
use ZF2x\Permissions\Acl\Event\AclEvent;
use ZF2x\Permissions\Acl\Identity\AclIdentityInterface;
use Zend\Permissions\Acl\Role\GenericRole;
use ZF2x\Permissions\Acl\Event\AclEventInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerInterface;
use Zend\Permissions\Acl\Acl;
use ZF2x\Identification\Event\IdentityEvent;

/**
 * Listen for Route event and check request parameters
 * against ACL for permission to dispatch.
 */
class AclListener
{
    /**
     * Event Manager
     * @var EventManagerInterface
     */
    protected $eventManager;

    /**
     * Acl Service
     * @var Acl
     */
    protected $acl;

    /**
     * [onIsAllowed description]
     * @param  AclEvent $e [description]
     * @return [type]      [description]
     */
    public function onIsAllowed(AclEvent $e)
    {
        $e->stopPropagation();
        $eventManager   = $this->getEventManager();
        $acl            = $this->getAcl();
        $resource       = $e->getResource();
        $privilege      = $e->getPrivilege();
        $role           = $e->getRole();
        if ($role) {
            return $acl->isAllowed($role, $resource, $privilege);
        }
        $identities = $eventManager->trigger(AclEvent::IDENTIFY_EVENT);
        foreach ($identities as $identity) {
            if ($identity instanceof AclIdentityInterface) {
                $role = new GenericRole($identity->getRole());
                if (!$acl->hasRole($role)) {
                    return false;
                }
                if (!$acl->hasResource($resource)) {
                    return false;
                }
                if ($acl->isAllowed($role, $resource, $privilege)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Gets the Event Manager.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->eventManager) {
            $this->setEventManager(new EventManager);
        }
        return $this->eventManager;
    }

    /**
     * Sets the Event Manager.
     *
     * @param EventManagerInterface $eventManager the eventManager
     *
     * @return self
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->eventManager = $eventManager;

        return $this;
    }

    /**
     * Gets the Acl Service.
     *
     * @return Acl
     */
    public function getAcl()
    {
        return $this->acl;
    }

    /**
     * Sets the Acl Service.
     *
     * @param Acl $acl the acl
     *
     * @return self
     */
    public function setAcl(Acl $acl)
    {
        $this->acl = $acl;

        return $this;
    }
}
