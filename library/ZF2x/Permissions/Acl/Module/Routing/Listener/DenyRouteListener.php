<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Module\Routing\Listener;

use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

/**
 * Listen for Deny Route Event and return a response
 */
class DenyRouteListener
{
    /**
     * Template Map
     * @var array
     */
    protected $templates = array(
        '401' => '401',
        '402' => '402',
        '403' => '403',
    );

    /**
     * Get Status Code
     * @param  MvcEvent $e Event
     * @return integer      status code
     */
    public function getStatusCode(MvcEvent $e)
    {
        //extend and add some logic on which to base statuscode decision
        return 403;
    }

    /**
     * On Deny Route
     * @param  MvcEvent $e Event
     * @return Response
     */
    public function onDenyRoute(MvcEvent $e)
    {
        $statusCode = $this->getStatusCode($e);
        $response = $e->getResponse();
        $response->setStatusCode($statusCode);
        if (!isset($this->templates[$statusCode])||$this->templates[$statusCode]===false) {
            return $response;
        }
        $model = new ViewModel(
            array(
                'resource' => $e->getRouteMatch()->getParam('controller'),
                'privilege' => $e->getRouteMatch()->getParam('action'),
            )
        );
        $model->setTemplate($this->templates[$statusCode]);
        $e->setViewModel($model);
        //$e->getViewModel()->addChild($model);
        $e->getApplication()->getEventManager()->trigger(MvcEvent::EVENT_RENDER, $e);
        return $response;
    }
}
