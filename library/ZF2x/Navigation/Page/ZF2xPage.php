<?php
/**
 * True Axiom Framework (http://framework.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/framework for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Navigation\Page;

use Zend\Navigation\Page\Mvc;
use Zend\Http\Request;

class ZF2xPage extends Mvc
{
    /**
     * Get Href
     * Match route against href to populate resource and privilege
     *
     * @return $href
     */
    public function getHref()
    {
        try {
            $this->setUseRouteMatch(true);
            $href = parent::getHref();
            $router = $this->getRouter();
            $routeName = $this->getRoute();
            $request = new Request;
            $request->setUri($href);
            $routeMatch = $router->match($request);
            $this->setResource($routeMatch->getParam('controller'));
            $this->setPrivilege($routeMatch->getParam('action'));
            return $href;
        } catch (\Exception $e) {
            //this is a hack that prevents missing parameters against segment routes
            //causing the failure of navigation acl check and rendering
            return '';
        }
    }

    /**
     * Get Resource
     * @return string
     */
    public function getResource()
    {
        if (!$this->hrefCache) {
            $this->getHref();
        }
        return $this->resource;
    }

    /**
     * Get Privilege
     * @return string
     */
    public function getPrivilege()
    {
        if (!$this->hrefCache) {
            $this->getHref();
        }
        return $this->privilege;
    }
}
