<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */
namespace ZF2x\Mandrill;

use Mandrill;
use ZF2x\Mail\Message;
use Zend\Mail\Headers;
use Zend\Mail\Header\Sender;
use Zend\Mail\Header\ReplyTo;
use Zend\Mail\Message as ZendMailMessage;
use ZF2x\Mail\Service as ZF2xMailService;
use ZF2x\Mandrill\Mail\Message as MandrillMessage;

class Service extends ZF2xMailService
{
    /**
     * Mandrill Api Client
     *
     * @var Mandrill
     */
    protected $mandrill;

    /**
     * Set Mandrill
     *
     * @param string $key
     */
    public function setMandrill(Mandrill $mandrill)
    {
        $this->mandrill = $mandrill;
    }

    /**
     * Create Message
     *
     * @return ZF2x\Mandrill\Mail\Message
     */
    public function createMessage()
    {
        //set default options @tbd
        return new MandrillMessage;
    }

    /**
     * Send Message
     *
     * @param  Message      $message
     * @param  boolean      $async
     * @param  null|string  $ipPool
     * @param  null|integer $sendAt
     *
     * @return array
     */
    public function sendMessage(ZendMailMessage $message, $async = false, $ipPool = null, $sendAt = null)
    {
        if (!$message instanceof Message) {
            $message = $this->baseMessageToZF2xMessage($message);
        }
        $message = $this->prepareMessage($message);
        $result = $this->mandrill->messages->send($message, $async, $ipPool, $sendAt);

        return $result;
    }

    /**
     * Prepare Message into required structure
     *
     * @param  Messsage $message
     *
     * @return string
     */
    public function prepareMessage(Message $message)
    {
        $data = array();
        $data['html'] = $message->getHtml();
        $data['text'] = $message->getText();
        $data['subject'] = $message->getSubject();
        $from = $message->getFrom();
        if (null !== $from) {
            $from = $from->rewind();
            $data['from_email'] = $from->getEmail();
            $data['from_name']  = $from->getName();
        } else {
            $data['from_email'] = null;
            $data['from_name']  = null;
        }
        $data['to'] = array();
        $to = $message->getTo();
        if (null !== $to) {
            foreach ($to as $address) {
                $row = array();
                $row['email'] = $address->getEmail();
                $row['name']  = $address->getName();
                $row['type']  = 'to';
                $data['to'][] = $row;
            }
        }
        $cc = $message->getCc();
        if (null !== $cc) {
            foreach ($cc as $address) {
                $row = array();
                $row['email'] = $address->getEmail();
                $row['name']  = $address->getName();
                $row['type']  = 'cc';
                $data['to'][] = $row;
            }
        }
        $bcc = $message->getCc();
        if (null !== $bcc) {
            foreach ($bcc as $address) {
                $row = array();
                $row['email'] = $address->getEmail();
                $row['name']  = $address->getName();
                $row['type']  = 'bcc';
                $data['to'][] = $row;
            }
        }
        $headers = $message->getHeaders();
        if (null === $headers) {
            $headers = new Headers;
        }
        $replyTo = $message->getReplyTo();
        if (null !== $replyTo) {
            $header = new ReplyTo;
            $header->setAddressList($replyTo);
            $headers->addHeader($header);
        }
        $sender = $message->getSender();
        if (null !== $sender) {
            $header = new Sender;
            $header->setAddress($sender);
            $headers->addHeader($header);
        }
        $data['headers'] = $headers->toArray();

        if ($message instanceof MandrillMessage) {

        }

        return $data;
    }
}
