<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\ORM\Doctrine\Options;

use Zend\Stdlib\AbstractOptions;

/**
 * Doctrine ORM Configuration Options
 */
class ConfigurationOptions extends AbstractOptions
{
    /**
     * Metadata Cache
     * @var string
     */
    protected $metadataCache    = 'Doctrine\Common\Cache\ArrayCache';

    /**
     * Query Cache
     * @var string
     */
    protected $queryCache       = 'Doctrine\Common\Cache\ArrayCache';

    /**
     * Result Cache
     * @var string
     */
    protected $resultCache      = 'Doctrine\Common\Cache\ArrayCache';

    /**
     * Sql Logger
     * @var string
     */
    protected $sqlLogger        = '';

    /**
     * Entity Paths
     * @var array
     */
    protected $entityPaths      = array();

    /**
     * Generate Proxies
     * @var boolean
     */
    protected $generateProxies  = true;

    /**
     * Proxy Dir
     * @var string
     */
    protected $proxyDirectory   = 'data/Doctrine/Proxy';

    /**
     * Proxy Namespace
     * @var string
     */
    protected $proxyNamespace   = 'ZF2x\Doctrine\Proxy';

    /**
     * Gets the Metadata Cache.
     *
     * @return string
     */
    public function getMetadataCache()
    {
        return $this->metadataCache;
    }

    /**
     * Sets the Metadata Cache.
     *
     * @param string $metadataCache the metadataCache
     *
     * @return self
     */
    public function setMetadataCache($metadataCache)
    {
        $this->metadataCache = $metadataCache;

        return $this;
    }

    /**
     * Gets the Query Cache.
     *
     * @return string
     */
    public function getQueryCache()
    {
        return $this->queryCache;
    }

    /**
     * Sets the Query Cache.
     *
     * @param string $queryCache the queryCache
     *
     * @return self
     */
    public function setQueryCache($queryCache)
    {
        $this->queryCache = $queryCache;

        return $this;
    }

    /**
     * Gets the Result Cache.
     *
     * @return string
     */
    public function getResultCache()
    {
        return $this->resultCache;
    }

    /**
     * Sets the Result Cache.
     *
     * @param string $resultCache the resultCache
     *
     * @return self
     */
    public function setResultCache($resultCache)
    {
        $this->resultCache = $resultCache;

        return $this;
    }

    /**
     * Gets the Sql Logger.
     *
     * @return string
     */
    public function getSqlLogger()
    {
        return $this->sqlLogger;
    }

    /**
     * Sets the Sql Logger.
     *
     * @param string $sqlLogger the sqlLogger
     *
     * @return self
     */
    public function setSqlLogger($sqlLogger)
    {
        $this->sqlLogger = $sqlLogger;

        return $this;
    }

    /**
     * Gets the Entity Paths.
     *
     * @return array
     */
    public function getEntityPaths()
    {
        return $this->entityPaths;
    }

    /**
     * Sets the Entity Paths.
     *
     * @param array $entityPaths the entityPaths
     *
     * @return self
     */
    public function setEntityPaths(array $entityPaths)
    {
        $this->entityPaths = $entityPaths;

        return $this;
    }

    /**
     * Gets the Generate Proxies.
     *
     * @return boolean
     */
    public function getGenerateProxies()
    {
        return $this->generateProxies;
    }

    /**
     * Sets the Generate Proxies.
     *
     * @param boolean $generateProxies the generateProxies
     *
     * @return self
     */
    public function setGenerateProxies($generateProxies)
    {
        $this->generateProxies = $generateProxies;

        return $this;
    }

    /**
     * Gets the Proxy Dir.
     *
     * @return string
     */
    public function getProxyDirectory()
    {
        return $this->proxyDirectory;
    }

    /**
     * Sets the Proxy Dir.
     *
     * @param string $proxyDirectory the proxyDirectory
     *
     * @return self
     */
    public function setProxyDirectory($proxyDirectory)
    {
        $this->proxyDirectory = $proxyDirectory;

        return $this;
    }

    /**
     * Gets the Proxy Namespace.
     *
     * @return string
     */
    public function getProxyNamespace()
    {
        return $this->proxyNamespace;
    }

    /**
     * Sets the Proxy Namespace.
     *
     * @param string $proxyNamespace the proxyNamespace
     *
     * @return self
     */
    public function setProxyNamespace($proxyNamespace)
    {
        $this->proxyNamespace = $proxyNamespace;

        return $this;
    }
}
