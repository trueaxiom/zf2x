<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Mail;

use SplQueue;
use Exception;
use Zend\Mime;
use Zend\Mail\Headers;
use Zend\Mail\Address;
use Zend\Mail\AddressList as ZendAddressList;
use Zend\View\Model\ModelInterface;
use Zend\Mail\Message as ZendMessage;
use Zend\View\Renderer\RendererInterface;

/**
 * Message
 */
class Message extends ZendMessage
{
    /**
     * Message encoding
     *
     * Used to determine whether or not to encode headers; defaults to ASCII.
     *
     * @var string
     */
    protected $encoding = 'ASCII';

    /**
     * Html Message
     *
     * @var string
     */
    protected $html;

    /**
     * Text Message
     *
     * @var string
     */
    protected $text;

    /**
     * Subject
     *
     * @var string
     */
    protected $subject;

    /**
     * From Address
     *
     * @var \Zend\Mail\AddressList
     */
    protected $from;

    /**
     * To Addresses
     *
     * @var \Zend\Mail\AddressList
     */
    protected $to;

    /**
     * CC Addresses
     *
     * @var \Zend\Mail\AddressList
     */
    protected $cc;

    /**
     * BCC Addresses
     *
     * @var \Zend\Mail\AddressList
     */
    protected $bcc;

    /**
     * Reply To Address
     *
     * @var \Zend\Mail\AddressList
     */
    protected $replyTo;

    /**
     * Sender Address
     *
     * @var \Zend\Mail\Address
     */
    protected $sender;

    /**
     * Attachments
     *
     * @var SplQueue
     */
    protected $attachments;

    /**
     * Images
     *
     * @var SplQueue
     */
    protected $images;

    /**
     * View Renderer
     *
     * @var Zend\View\Renderer\RendererInterface
     */
    protected $renderer;

    /**
     * Message Body
     *
     * @var Zend\Mime\Message
     */
    protected $body;

    /**
     * Message Headers
     *
     * @var Zend\Mail\Headers
     */
    protected $headers;

    /**
     * Initialize default state
     */
    public function __construct()
    {
        $attachments = new SplQueue;
        $this->setAttachments($attachments);
        $images = new SplQueue;
        $this->setImages($images);
    }

    /**
     * Prepare Message Body and Headers For \Zend\Mail\TransportInterface implementations
     *
     * @return self
     */
    public function prepare()
    {
        $headers = parent::getHeaders();
        $from = $this->getFrom();
        if (null !== $from) {
            $header = $this->getHeaderByName('from', 'Zend\Mail\Header\From');
            $header->setAddressList($from);
        }
        $to = $this->getTo();
        if (null !== $to) {
            $header = $this->getHeaderByName('to', 'Zend\Mail\Header\To');
            $header->setAddressList($to);
        }
        $cc = $this->getCc();
        if (null !== $cc) {
            $header = $this->getHeaderByName('cc', 'Zend\Mail\Header\Cc');
            $header->setAddressList($cc);
        }
        $bcc = $this->getBcc();
        if (null !== $bcc) {
            $header = $this->getHeaderByName('bcc', 'Zend\Mail\Header\Bcc');
            $header->setAddressList($bcc);
        }
        $replyTo = $this->getReplyTo();
        if (null !== $replyTo) {
            $header = $this->getHeaderByName('reply-to', 'Zend\Mail\Header\ReplyTo');
            $header->setAddressList($replyTo);
        }
        $sender = $this->getSender();
        if (null !== $sender) {
            parent::setSender($sender);
        }
        $subject = $this->getSubject();
        if (null !== $subject) {
            parent::setSubject($subject);
        }
        $html = $this->getHtml();
        if (null !== $html) {
            $this->prepareHtmlBodyPart($html);
        }
        $text = $this->getText();
        if (null !== $text) {
            $this->prepareTextBodyPart($text);
        }
        $attachments = $this->getAttachments();
        if (!$attachments->isEmpty()) {
            $this->prepareAttachmentBodyParts($attachments);
        }
        $images = $this->getImages();
        if (!$images->isEmpty()) {
            $this->prepareImageBodyParts($images);
        }
        if ($this->hasBody()) {
            parent::setBody($this->getBody());
        }
        return $this;
    }

    /**
     * Prepare Html Body Part
     *
     * @param  string $content
     *
     * @return self
     */
    public function prepareHtmlBodyPart($content)
    {
        $body           = $this->getBody();
        $html           = new Mime\Part($content);
        $html->type     = Mime\Mime::TYPE_HTML;
        $html->encoding = Mime\Mime::ENCODING_7BIT;
        $body->addPart($html);

        return $this;
    }

    /**
     * Prepare Text Body Parts
     *
     * @param  string $content
     *
     * @return self
     */
    public function prepareTextBodyPart($content)
    {
        $body               = $this->getBody();
        $plain              = new Mime\Part($content);
        $plain->type        = Mime\Mime::TYPE_TEXT;
        $plain->encoding    = Mime\Mime::ENCODING_QUOTEDPRINTABLE;
        $body->addPart($plain);

        return $this;
    }

    /**
     * Prepare Attachment Body Parts
     *
     * @param  SplQueue $attachments
     *
     * @return self
     */
    public function prepareAttachmentBodyParts(SplQueue $attachments)
    {
        $body = $this->getBody();
        foreach ($attachments as $file) {
            $attachment                 = new Mime\Part($file);
            $attachment->type           = $mime;
            $attachment->filename       = $filename;
            $attachment->encoding       = Mime\Mime::ENCODING_BASE64;
            $attachment->disposition    = Mime\Mime::DISPOSITION_ATTACHMENT;
            $body->addPart($attachment);
        }

        return $this;
    }

    /**
     * Prepare Image Body Parts
     *
     * @param  SplQueue $images
     *
     * @return self
     */
    public function prepareImageBodyParts(SplQueue $images)
    {
        return $this->prepareAttachmentBodyParts($images);
    }

    /**
     * Gets the Message encoding
     *
     * @return string
     */
    public function getEncoding()
    {
        return $this->encoding;
    }

    /**
     * Sets the Message encoding
     *
     * @param string $encoding the encoding
     *
     * @return self
     */
    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;

        return $this;
    }

    /**
     * Gets the Html Message.
     *
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Sets the Html Message.
     *
     * @param string $html the html
     *
     * @return self
     */
    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    /**
     * Set Html Content By Rendering ModelInterface
     *
     * @param ModelInterface $model
     */
    public function setHtmlViewModel(ModelInterface $model)
    {
        $renderer = $this->getRenderer();
        if (!$renderer) {
            throw new Exception("Renderer Required");
        }
        try {
            $html = $renderer->render($model);
        } catch (Exception $e) {
            throw $e;
        }
        $this->setHtml($html);
    }

    /**
     * Gets the Text Message.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets the Text Message.
     *
     * @param string $text the text
     *
     * @return self
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Gets the Subject.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Sets the Subject.
     *
     * @param string $subject the subject
     *
     * @return self
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Gets the From Address.
     *
     * @return \Zend\Mail\AddressList
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Sets the From Address.
     *
     * @param \Zend\Mail\AddressList $from the from
     *
     * @return self
     */
    public function setFrom($from, $name = null)
    {
        $this->setFromAddressList($from);

        return $this;
    }

    /**
     * Set From Address List
     *
     * @param \Zend\Mail\AddressList $from
     */
    public function setFromAddressList(ZendAddressList $from)
    {
        $this->from = $from;
    }

    /**
     * Gets the To Addresses.
     *
     * @return \Zend\Mail\AddressList
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Sets the To Addresses.
     *
     * @param \Zend\Mail\AddressList $to the to
     *
     * @return self
     */
    public function setTo($to, $name = null)
    {
        $this->setToAddressList($to);

        return $this;
    }

    /**
     * Set To Address List
     *
     * @param \Zend\Mail\AddressList $to
     */
    public function setToAddressList(ZendAddressList $to)
    {
        $this->to = $to;
    }

    /**
     * Gets the CC Addresses.
     *
     * @return \Zend\Mail\AddressList
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * Sets the CC Addresses.
     *
     * @param \Zend\Mail\AddressList $cc the cc
     *
     * @return self
     */
    public function setCc($cc, $name = null)
    {
        $this->setCcAddressList($cc);

        return $this;
    }

    /**
     * Set CC Address List
     *
     * @param \Zend\Mail\AddressList $cc
     */
    public function setCcAddressList(ZendAddressList $cc)
    {
        $this->cc = $cc;
    }

    /**
     * Gets the BCC Addresses.
     *
     * @return \Zend\Mail\AddressList
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * Sets the BCC Addresses.
     *
     * @param \Zend\Mail\AddressList $bcc the bcc
     *
     * @return self
     */
    public function setBcc($bcc, $name = null)
    {
        $this->setBccAddressList($bcc);

        return $this;
    }

    /**
     * Set Bcc Address List
     *
     * @param \Zend\Mail\AddressList $bcc
     */
    public function setBccAddressList(ZendAddressList $bcc)
    {
        $this->bcc = $bcc;
    }

    /**
     * Gets the Sender Address.
     *
     * @return \Zend\Mail\Address
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Sets the Sender Address.
     *
     * @param \Zend\Mail\Address $sender the sender
     *
     * @return self
     */
    public function setSender($sender, $name = null)
    {
        $this->setSenderAddress($sender);

        return $this;
    }

    /**
     * Sets the Sender Address
     *
     * @param \Zend\Mail\Address $sender the sender
     *
     * @return self
     */
    public function setSenderAddress(Address $sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Gets the Attachments.
     *
     * @return SplQueue
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Sets the Attachments.
     *
     * @param SplQueue $attachments the attachments
     *
     * @return self
     */
    public function setAttachments(SplQueue $attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Gets the Images.
     *
     * @return SplQueue
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets the Images.
     *
     * @param SplQueue $images the images
     *
     * @return self
     */
    public function setImages(SplQueue $images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Gets the View Renderer.
     *
     * @return Zend\View\Renderer\RendererInterface
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * Sets the View Renderer.
     *
     * @param Zend\View\Renderer\RendererInterface $renderer the renderer
     *
     * @return self
     */
    public function setRenderer(RendererInterface $renderer)
    {
        $this->renderer = $renderer;

        return $this;
    }

    public function hasBody()
    {
        return $this->body instanceof Mime\Message;
    }

    /**
     * Gets the Message Body.
     *
     * @return Zend\Mime\Message
     */
    public function getBody()
    {
        if (null === $this->body) {
            $this->setBody(new Mime\Message);
        }
        return $this->body;
    }

    /**
     * Sets the Message Body.
     *
     * @param Zend\Mime\Message $body the body
     *
     * @return self
     */
    public function setBody($body)
    {
        $this->setMessageBody($body);

        return $this;
    }

    /**
     * Set Message Body
     *
     * @param \Zend\Mime\Message $body
     */
    public function setMessageBody(Mime\Message $body)
    {
        $this->body = $body;
    }

    /**
     * Sets the Message Headers.
     *
     * @param Zend\Mail\Headers $headers the headers
     *
     * @return self
     */
    public function setHeaders(Headers $headers)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Gets the Message Headers.
     *
     * @return Zend\Mail\Headers
     */
    public function getHeaders()
    {
        return $this->headers;
    }


    /**
     * Gets the Reply To Address.
     *
     * @return \Zend\Mail\Address
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * Sets the Reply To Address
     *
     * @param \Zend\Mail\Address $replyTo the reply to
     *
     * @return self
     */
    public function setReplyTo($replyTo, $name = null)
    {
        $this->setReplyToAddressList($replyTo);

        return $this;
    }

    /**
     * Sets the Reply To Address List
     *
     * @param \Zend\Mail\Address $replyTo the reply to
     *
     * @return self
     */
    public function setReplyToAddressList(ZendAddressList $replyTo)
    {
        $this->replyTo = $replyTo;

        return $this;
    }
}
