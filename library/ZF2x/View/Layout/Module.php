<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\View\Layout;

use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $application = $e->getTarget();
        $eventManager = $application->getEventManager();
        $serviceManager = $application->getServiceManager();
        $conf = $serviceManager->get('config');
        if (!isset($conf['layout'])) {
            return;
        }
        $conf = $conf['layout'];
        //Modify layout based on Controller Namespace
        if (isset($conf['module_layout']) && is_array($conf['module_layout'])) {
            $moduleLayout = $conf['module_layout'];
            $eventManager->attach(
                MvcEvent::EVENT_DISPATCH,
                function ($e) use ($moduleLayout) {
                    $class = get_class($e->getTarget());
                    foreach ($moduleLayout as $ns => $layout) {
                        if (strpos($class, $ns) !== false) {
                            $e->getTarget()->layout($layout);
                        }
                    }
                }
            );
        }
        //Stack Views based on Controller Namespace
        if (isset($conf['stacked_module_layout']) && is_array($conf['stacked_module_layout'])) {
            $moduleLayout = $conf['stacked_module_layout'];
            $eventManager->attach(
                MvcEvent::EVENT_DISPATCH,
                function ($e) use ($moduleLayout) {
                    $class = get_class($e->getTarget());
                    foreach ($moduleLayout as $ns => $layout) {
                        if (strpos($class, $ns) !== false) {
                            $vm = $e->getViewModel();
                            $vmc = $vm->getChildren();
                            $vmn = new ViewModel;
                            $vmn->setTemplate($layout);
                            foreach ($vmc as $v) {
                                $vmn->addChild($v);
                            }
                            $vm->clearChildren();
                            $vm->addChild($vmn);
                        }
                    }
                },
                -1000
            );
        }
        //Use custom template listener if specified
        if (isset($conf['template_listener'])) {
            if ($conf['template_listener'] instanceof \Zend\Mvc\View\Http\InjectTemplateListener) {
                $listener = $conf['template_listener'];
            } else {
                $listener = $serviceManager->get($conf['template_listener']);
            }
            $eventManager->attach(MvcEvent::EVENT_DISPATCH, array($listener, 'injectTemplate'));
        }
    }
}
