<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Authentication\Identification\Manager;

use ZF2x\Identification\Manager\AbstractIdentityManager;
use ZF2x\Identification\Module\Config\Mapper\AuthEntityConfigMapper;

class AuthIdentityManager extends AbstractIdentityManager
{
    const IDENTITY_CLASS = 'ZF2x\Authentication\Identification\Identity\AuthIdentity';
    const SESSION_NAMESPACE = 'auth';
}
