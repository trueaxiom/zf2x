<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Identification\Event;

use Zend\Authentication\Adapter\AdapterInteface;
use ZF2x\Identification\Identity\IdentityInterface;

interface IdentityEventInterface
{
    /**
     * Get Identity
     * @return AuthIdentityInterface
     */
    public function getIdentity();

    /**
     * Set Identity
     * @param AuthIdentityInterface $identity Identity
     */
    public function setIdentity(IdentityInterface $identity);

    /**
     * Get Identity Adapter
     * @return AuthAdapterInterface
     */
    public function getAdapter();

    /**
     * Set Adapter
     * @param AdapterInterface $adapter AdapterInterface
     */
    public function setAdapter(AdapterInterface $adapter);
}
