<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\ORM\Doctrine\Factory;

use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Entity Manager Factory
 */
class EntityManagerFactory implements FactoryInterface
{
    /**
     * Create Entity Manager
     * @param  ServiceLocatorInterface $serviceLocator Service Locator
     * @return EntityManager    Entity Manager
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $connection = $serviceLocator->get('DoctrineORMConnectionOptions');
        $configuration = $serviceLocator->get('DoctrineORMConfiguration');
        return EntityManager::create($connection->toArray(), $configuration);
    }
}
