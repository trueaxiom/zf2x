<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl;

use Zend\Stdlib\AbstractOptions;

/**
 * Options class for Acl Factory
 */
class Options extends AbstractOptions
{
    /**
     * Acl Roles
     *
     * @var array
     */
    protected $roles = array();

    /**
     * Acl Resources
     *
     * @var array
     */
    protected $resources = array();

    /**
     * Acl Allow Rules
     *
     * @var array
     */
    protected $allow = array();

    /**
     * Acl Deny Rules
     *
     * @var array
     */
    protected $deny = array();

    /**
     * Default Role
     *
     * @var string
     */
    protected $defaultRole = 'guest';

    /**
     * Return Roles
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set Roles
     *
     * @param array $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * Return Resources
     *
     * @return array
     */
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * Set Resources
     *
     * @param array $resources
     */
    public function setResources($resources)
    {
        $this->resources = $resources;
        return $this;
    }

    /**
     * Return Allow Rules
     *
     * @return array
     */
    public function getAllow()
    {
        return $this->allow;
    }

    /**
     * Set Allow Rules
     *
     * @param array $allow
     */
    public function setAllow($allow)
    {
        $this->allow = $allow;
        return $this;
    }

    /**
     * Return Deny Rules
     *
     * @return array
     */
    public function getDeny()
    {
        return $this->deny;
    }

    /**
     * Set Deny Rules
     *
     * @param array $deny
     */
    public function setDeny($deny)
    {
        $this->deny = $deny;
        return $this;
    }

    /**
     * Gets the Default Role.
     *
     * @return string
     */
    public function getDefaultRole()
    {
        return $this->defaultRole;
    }

    /**
     * Sets the Default Role.
     *
     * @param string $defaultRole the default role
     *
     * @return self
     */
    public function setDefaultRole($defaultRole)
    {
        $this->defaultRole = $defaultRole;

        return $this;
    }
}
