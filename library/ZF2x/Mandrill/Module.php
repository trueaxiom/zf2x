<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */
namespace ZF2x\Mandrill;

use Mandrill;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

class Module implements ServiceProviderInterface
{
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'zf2x.mandrill.options' => 'ZF2x\Mandrill\OptionsFactory',
                'mandrill' => function ($sl) {
                    $options = $sl->get('zf2x.mandrill.options');
                    $apikey = $options->getApiKey();
                    $mandrill = new Mandrill($apikey);
                    return $mandrill;
                },
                'zf2x.mandrill.mail.service' => function ($sl) {
                    $service = new Service;
                    $mandrill = $sl->get('mandrill');
                    $service->setMandrill($mandrill);
                    return $service;
                }
            ),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getConfig()
    {
        return include __DIR__ . '/../../../config/module.config.php';
    }
}
