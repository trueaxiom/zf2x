<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Module\RoleEntityConfigMapper;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use ZF2x\Permissions\Acl\Module\RoleEntityConfigMapper\Mapper\RoleEntityConfigMapper;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return array(
            'service_manager' => array(
                'factories' => array(
                    'RoleEntityMapperOptions' => 'ZF2x\Permissions\Acl\Module\RoleEntityConfigMapper\OptionsFactory',
                    'AuthRoleEntityMapper' => function ($sl) {
                        $mapper         = new RoleEntityConfigMapper;
                        $options        = $sl->get('RoleEntityMapperOptions');
                        $identities     = $options->getIdentities();
                        $mapper->setAuthRoles($identities);
                        return $mapper;
                    }
                ),
            )
        );
    }
}
