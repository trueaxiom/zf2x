<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Authentication\Mapper\AuthEntity\DoctrineORM;

use Doctrine\ORM\EntityRepository;
use ZF2x\Authentication\Mapper\AuthEntity\AuthEntityMapperInterface;

class AuthEntityMapper extends EntityRepository implements AuthEntityMapperInterface
{

    public function findOneByAuthId($authId)
    {
        return $this->findOneBy(array('id' => $authId));
    }

    public function findOneByIdentity($identity)
    {
        return $this->findOneBy(array('identity' => $identity));
    }

    public function create($identity, $credential)
    {
        $class = $this->getEntityName();
        $auth = new $class;
        $auth->setIdentity($identity);
        $auth->setCredential($credential);
        $this->_em->persist($auth);
        $this->_em->flush();
        return $auth;
    }
}
