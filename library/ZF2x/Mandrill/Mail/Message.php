<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */
namespace ZF2x\Mandrill\Mail;

use ZF2x\Mail\Message as ZF2xMessage;

/**
 * Mandrill Specific Message Properties
 */
class Message extends ZF2xMessage
{
    /**
     * Whether or not this message is important,
     * and should be delivered ahead of non-important messages
     *
     * @var boolean
     */
    protected $important;

    /**
     * Whether or not to turn on open tracking for the message
     *
     * @var boolean
     */
    protected $trackOpens;

    /**
     * Whether or not to turn on click tracking for the message
     *
     * @var boolean
     */
    protected $trackClicks;

    /**
     * Whether or not to automatically generate a text part for messages that are not given text
     *
     * @var boolean
     */
    protected $autoText;

    /**
     * Whether or not to automatically generate an HTML part for messages that are not given HTML
     *
     * @var boolean
     */
    protected $autoHtml;

    /**
     * Whether or not to automatically inline all CSS styles provided in the message HTML
     * - only for HTML documents less than 256KB in size
     *
     * @var boolean
     */
    protected $inlineCss;

    /**
     * Whether or not to strip the query string from URLs when aggregating tracked URL data
     *
     * @var boolean
     */
    protected $useStripQs;

    /**
     * Whether or not to expose all recipients in to "To" header for each email
     *
     * @var boolean
     */
    protected $preserveRecipients;

    /**
     * Content logging for sensitive emails
     *
     * @var boolean
     */
    protected $viewContentLink = true;

    /**
     * An optional address to receive an exact copy of each recipient's email
     *
     * @var string
     */
    protected $bccAddress;

    /**
     * A custom domain to use for tracking opens and clicks instead of mandrillapp.com
     *
     * @var string
     */
    protected $trackingDomain;

    /**
     * A custom domain to use for SPF/DKIM signing instead of mandrill
     * (for "via" or "on behalf of" in email clients)
     *
     * @var string
     */
    protected $signingDomain;

    /**
     * A custom domain to use for the messages's return-path
     *
     * @var string
     */
    protected $returnPathDomain;

    /**
     * Whether to evaluate merge tags in the message.
     * Will automatically be set to true if either merge_vars or global_merge_vars are provided.
     *
     * @var boolean
     */
    protected $merge;

    /**
     * Global merge variables to use for all recipients. You can override these per recipient.
     *
     * array(
     *     array(
     *         'name'    => the global merge variable's name, case-insensitive and may not start with _
     *         'content' => the global merge variable's content
     *     )
     * )
     *
     * @var array
     */
    protected $globalMergeVars;

    /**
     * Per-recipient merge variables which override global merge variables with the same name.
     *
     * array(
     *     'recpt*' => 'REQUIRED - the email address of the recipient that the merge variables should apply to',
     *     'vars' => array(
     *         array(
     *             'name'    => the global merge variable's name, case-insensitive and may not start with _
     *             'content' => the global merge variable's content
     *         )
     *     )
     * )
     * @var array
     */
    protected $mergeVars;

    /**
     * Tag - An array of string to tag the message with. Stats are accumulated using tags,
     * though we only store the first 100 we see, so this should not be unique or change frequently.
     * Tags should be 50 characters or less. Any tags starting with an underscore are reserved
     * for internal use and will cause errors.
     *
     * @var array
     */
    protected $tags;

    /**
     * The unique id of a subaccount for this message - must already exist or will fail with an error
     *
     * @var string
     */
    protected $subaccount;

    /**
     * An array of strings indicating for which any matching URLs will automatically have
     * Google Analytics parameters appended to their query string automatically.
     *
     * @var array
     */
    protected $googleAnalyticsDomains;

    /**
     * Optional string indicating the value to set for the utm_campaign tracking parameter.
     * If this isn't provided the email's from address will be used instead.
     *
     * @var array|string
     */
    protected $googleAnalyticsCampain;

    /**
     * Metadata an associative array of user metadata.
     * Mandrill will store this metadata and make it available for retrieval.
     * In addition, you can select up to 10 metadata fields to index and make searchable using the Mandrill search api.
     *
     * @var array
     */
    protected $metadata;

    /**
     * Per-recipient metadata that will override the global values specified in the metadata parameter.
     *
     * array(
     *     array(
     *         'rcpt' => 'the email address of the recipient that the metadata is associated with',
     *         'values' => array(
     *             'An' => 'associated array containing the recipient's unique metadata',
     *             'If' => key exists in per-recipient metadata and global metadata, per-recipient metadataused.'
     *         ).
     *     )
     * )
     * @var array
     */
    protected $recipientMetadata;

    /**
     * Gets whether or not this message is important, and should be delivered ahead of non-important messages.
     *
     * @return boolean
     */
    public function getImportant()
    {
        return $this->important;
    }

    /**
     * Sets whether or not this message is important, and should be delivered ahead of non-important messages.
     *
     * @param boolean $important the important
     *
     * @return self
     */
    public function setImportant($important)
    {
        $this->important = $important;

        return $this;
    }

    /**
     * Gets whether or not to turn on open tracking for the message.
     *
     * @return boolean
     */
    public function getTrackOpens()
    {
        return $this->trackOpens;
    }

    /**
     * Sets whether or not to turn on open tracking for the message.
     *
     * @param boolean $trackOpens the track opens
     *
     * @return self
     */
    public function setTrackOpens($trackOpens)
    {
        $this->trackOpens = $trackOpens;

        return $this;
    }

    /**
     * Gets whether or not to turn on click tracking for the message.
     *
     * @return boolean
     */
    public function getTrackClicks()
    {
        return $this->trackClicks;
    }

    /**
     * Sets whether or not to turn on click tracking for the message.
     *
     * @param boolean $trackClicks the track clicks
     *
     * @return self
     */
    public function setTrackClicks($trackClicks)
    {
        $this->trackClicks = $trackClicks;

        return $this;
    }

    /**
     * Gets whether or not to automatically generate a text part for messages that are not given text.
     *
     * @return boolean
     */
    public function getAutoText()
    {
        return $this->autoText;
    }

    /**
     * Sets whether or not to automatically generate a text part for messages that are not given text.
     *
     * @param boolean $autoText the auto text
     *
     * @return self
     */
    public function setAutoText($autoText)
    {
        $this->autoText = $autoText;

        return $this;
    }

    /**
     * Gets whether or not to automatically generate an HTML part for messages that are not given HTML.
     *
     * @return boolean
     */
    public function getAutoHtml()
    {
        return $this->autoHtml;
    }

    /**
     * Sets whether or not to automatically generate an HTML part for messages that are not given HTML.
     *
     * @param boolean $autoHtml the auto html
     *
     * @return self
     */
    public function setAutoHtml($autoHtml)
    {
        $this->autoHtml = $autoHtml;

        return $this;
    }

    /**
     * Gets whether or not to automatically inline all CSS styles provided in the message HTML
     *
     * @return boolean
     */
    public function getInlineCss()
    {
        return $this->inlineCss;
    }

    /**
     * Sets whether or not to automatically inline all CSS styles provided in the message HTML
     *
     * @param boolean $inlineCss the inline css
     *
     * @return self
     */
    public function setInlineCss($inlineCss)
    {
        $this->inlineCss = $inlineCss;

        return $this;
    }

    /**
     * Gets whether or not to strip the query string from URLs when aggregating tracked URL data.
     *
     * @return boolean
     */
    public function getUseStripQs()
    {
        return $this->useStripQs;
    }

    /**
     * Sets whether or not to strip the query string from URLs when aggregating tracked URL data.
     *
     * @param boolean $useStripQs the use strip qs
     *
     * @return self
     */
    public function setUseStripQs($useStripQs)
    {
        $this->useStripQs = $useStripQs;

        return $this;
    }

    /**
     * Gets whether or not to expose all recipients in to "To" header for each email.
     *
     * @return boolean
     */
    public function getPreserveRecipients()
    {
        return $this->preserveRecipients;
    }

    /**
     * Sets whether or not to expose all recipients in to "To" header for each email.
     *
     * @param boolean $preserveRecipients the preserve recipients
     *
     * @return self
     */
    public function setPreserveRecipients($preserveRecipients)
    {
        $this->preserveRecipients = $preserveRecipients;

        return $this;
    }

    /**
     * Gets content logging for sensitive emails.
     *
     * @return boolean
     */
    public function getViewContentLink()
    {
        return $this->viewContentLink;
    }

    /**
     * Sets content logging for sensitive emails.
     *
     * @param boolean $viewContentLink the view content link
     *
     * @return self
     */
    public function setViewContentLink($viewContentLink)
    {
        $this->viewContentLink = $viewContentLink;

        return $this;
    }

    /**
     * Gets optional address to receive an exact copy of each recipient's email.
     *
     * @return string
     */
    public function getBccAddress()
    {
        return $this->bccAddress;
    }

    /**
     * Sets optional address to receive an exact copy of each recipient's email.
     *
     * @param string $bccAddress the bcc address
     *
     * @return self
     */
    public function setBccAddress($bccAddress)
    {
        $this->bccAddress = $bccAddress;

        return $this;
    }

    /**
     * Gets custom domain to use for tracking opens and clicks instead of mandrillapp.com.
     *
     * @return string
     */
    public function getTrackingDomain()
    {
        return $this->trackingDomain;
    }

    /**
     * Sets custom domain to use for tracking opens and clicks instead of mandrillapp.com.
     *
     * @param string $trackingDomain the tracking domain
     *
     * @return self
     */
    public function setTrackingDomain($trackingDomain)
    {
        $this->trackingDomain = $trackingDomain;

        return $this;
    }

    /**
     * Gets custom domain to use for SPF/DKIM signing instead of mandrill
     *
     * @return string
     */
    public function getSigningDomain()
    {
        return $this->signingDomain;
    }

    /**
     * Sets custom domain to use for SPF/DKIM signing instead of mandrill
     *
     * @param string $signingDomain the signing domain
     *
     * @return self
     */
    public function setSigningDomain($signingDomain)
    {
        $this->signingDomain = $signingDomain;

        return $this;
    }

    /**
     * Gets custom domain to use for the messages's return-path.
     *
     * @return string
     */
    public function getReturnPathDomain()
    {
        return $this->returnPathDomain;
    }

    /**
     * Sets custom domain to use for the messages's return-path.
     *
     * @param string $returnPathDomain the return path domain
     *
     * @return self
     */
    public function setReturnPathDomain($returnPathDomain)
    {
        $this->returnPathDomain = $returnPathDomain;

        return $this;
    }

    /**
     * Gets whether to evaluate merge tags in the message.
     *
     * @return boolean
     */
    public function getMerge()
    {
        return $this->merge;
    }

    /**
     * Sets whether to evaluate merge tags in the message.
     *
     * @param boolean $merge the merge
     *
     * @return self
     */
    public function setMerge($merge)
    {
        $this->merge = $merge;

        return $this;
    }

    /**
     * Gets the Global merge variables to use for all recipients. You can override these per recipient.
     *
     * @return array
     */
    public function getGlobalMergeVars()
    {
        return $this->globalMergeVars;
    }

    /**
     * Sets the Global merge variables to use for all recipients. You can override these per recipient.
     *
     * @param array $globalMergeVars the global merge vars
     *
     * @return self
     */
    public function setGlobalMergeVars(array $globalMergeVars)
    {
        $this->globalMergeVars = $globalMergeVars;

        return $this;
    }

    /**
     * Gets the Per-recipient merge variables which override global merge variables with the same name.
     *
     * @return array
     */
    public function getMergeVars()
    {
        return $this->mergeVars;
    }

    /**
     * Sets the Per-recipient merge variables which override global merge variables with the same name.
     *
     * @param array $mergeVars the merge vars
     *
     * @return self
     */
    public function setMergeVars(array $mergeVars)
    {
        $this->mergeVars = $mergeVars;

        return $this;
    }

    /**
     * Gets the Tag - An array of string to tag the message with. Stats are accumulated using tags,
     *
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Sets the Tag - An array of string to tag the message with. Stats are accumulated using tags,
     *
     * @param array $tags the tags
     *
     * @return self
     */
    public function setTags(array $tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Gets the unique id of a subaccount for this message - must already exist or will fail with an error.
     *
     * @return string
     */
    public function getSubaccount()
    {
        return $this->subaccount;
    }

    /**
     * Sets the unique id of a subaccount for this message - must already exist or will fail with an error.
     *
     * @param string $subaccount the subaccount
     *
     * @return self
     */
    public function setSubaccount($subaccount)
    {
        $this->subaccount = $subaccount;

        return $this;
    }

    /**
     * Gets an array of strings indicating for which any matching URLs will automatically have
     *
     * @return array
     */
    public function getGoogleAnalyticsDomains()
    {
        return $this->googleAnalyticsDomains;
    }

    /**
     * Sets an array of strings indicating for which any matching URLs will automatically have
     *
     * @param array $googleAnalyticsDomains the google analytics domains
     *
     * @return self
     */
    public function setGoogleAnalyticsDomains(array $googleAnalyticsDomains)
    {
        $this->googleAnalyticsDomains = $googleAnalyticsDomains;

        return $this;
    }

    /**
     * Gets the Optional string indicating the value to set for the utm_campaign tracking parameter.
     *
     * @return array|string
     */
    public function getGoogleAnalyticsCampain()
    {
        return $this->googleAnalyticsCampain;
    }

    /**
     * Sets the Optional string indicating the value to set for the utm_campaign tracking parameter.
     *
     * @param array|string $googleAnalyticsCampain the google analytics campain
     *
     * @return self
     */
    public function setGoogleAnalyticsCampain($googleAnalyticsCampain)
    {
        $this->googleAnalyticsCampain = $googleAnalyticsCampain;

        return $this;
    }

    /**
     * Gets the Metadata an associative array of user metadata.
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Sets the Metadata an associative array of user metadata.
     *
     * @param array $metadata the metadata
     *
     * @return self
     */
    public function setMetadata(array $metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Gets the Per-recipient metadata that will override the global values specified in the metadata parameter.
     *
     * @return array
     */
    public function getRecipientMetadata()
    {
        return $this->recipientMetadata;
    }

    /**
     * Sets the Per-recipient metadata that will override the global values specified in the metadata parameter.
     *
     * @param array $recipientMetadata the recipient metadata
     *
     * @return self
     */
    public function setRecipientMetadata(array $recipientMetadata)
    {
        $this->recipientMetadata = $recipientMetadata;

        return $this;
    }
}
