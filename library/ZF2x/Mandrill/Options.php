<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */
namespace ZF2x\Mandrill;

use Zend\Stdlib\AbstractOptions;

class Options extends AbstractOptions
{
    /**
     * Mandrill API Key
     *
     * @var string
     */
    protected $apiKey;

    /**
     * Gets the Mandrill API Key.
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Sets the Mandrill API Key.
     *
     * @param string $apiKey the api key
     *
     * @return self
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }
}
