<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Mvc\View\Http;

use Zend\Mvc\MvcEvent;
use Zend\View\Model\ModelInterface as ViewModel;
use Zend\Mvc\View\Http\InjectTemplateListener as TemplateListener;

/**
 * Inject Template Listener
 */
class InjectTemplateListener extends TemplateListener
{
    /**
     * Inject Template Listener stips Controller from namespace
     * @param  MvcEvent $e Event
     * @return null
     */
    public function injectTemplate(MvcEvent $e)
    {
        $model = $e->getResult();
        if (!$model instanceof ViewModel) {
            return;
        }
        $controller = get_class($e->getTarget());
        $template = '';
        $parts = explode('\\', $controller);
        foreach ($parts as $i => $part) {
            //strip Controller from Namespace
            if ('Controller' === $part) {
                continue;
            }
            //strip Controller from Namespace Part
            if (strpos($part, 'Controller')) {
                $part = $this->inflectName(substr($part, 0, strpos($part, 'Controller')));
            }
            $template .= $this->inflectName($part).'/';
        }
        $template .= $this->inflectName($e->getRouteMatch()->getParam('action'));

        $model->setTemplate($this->inflectName($template));
    }
}
