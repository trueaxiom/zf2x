Mail
====

The ZF2x Mail component attempts to simplify Emailing functionality. The Zend Framework 2 Mail\Message class is very much designed for usage with a classic type Transport, i.e SMTP. The ZF2x\Mail\Message class is intended to provide equivalent functionality but with an interface easier for more modern API based transports to consume.
