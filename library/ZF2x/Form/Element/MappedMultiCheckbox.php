<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Form\Element;

use Zend\Form\Element\MultiCheckbox;
use ZF2x\Form\ValueOptionsMapperInterface;

class MappedMultiCheckbox extends MultiCheckbox
{
    /**
     * Mapper Interface
     * @var ValueOptionsMapperInterface
     */
    protected $mapper;

    /**
     * Set Value Options
     * @param array $valueOptions
     */
    public function setValueOptions(array $valueOptions = array())
    {
        $this->setUseHiddenElement(true);
        $mapper = $this->getMapper();
        $valueOptions = $mapper->getValueOptions();
        parent::setValueOptions($valueOptions);
    }

    /**
     * Gets the Mapper Interface.
     *
     * @return ValueOptionsMapperInterface
     */
    public function getMapper()
    {
        return $this->mapper;
    }

    /**
     * Sets the Mapper Interface.
     *
     * @param ValueOptionsMapperInterface $mapper the mapper
     *
     * @return self
     */
    public function setMapper(ValueOptionsMapperInterface $mapper)
    {
        $this->mapper = $mapper;

        return $this;
    }
}
