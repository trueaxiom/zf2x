<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\View\Helper\BootstrapForm;

use Zend\View\Helper\AbstractHelper;
use Zend\Form\Form as ZendForm;
use Zend\Form\Fieldset;

class Form extends AbstractHelper
{
    public function __invoke(ZendForm $form, $style = 'vertical', $fieldsetclass = null)
    {
        if ($style) {
            $form->setAttribute('class', $form->getAttribute('class') . ' form-' . $style);
        }
        $form->prepare();
        $output = '';
        $output .= $this->view->form()->openTag($form);
        if($form->getName()){
            $output .= '<legend>'.$form->getName().'</legend>';
        }
        $elements = $form->getIterator();
        foreach ($elements as $key => $element) {
            if ($element instanceof Fieldset) {
                $output .= $this->view->bootstrapCollection($element, $style, true,$fieldsetclass);
            } else {
                $output .= $this->view->bootstrapRow($element, $style);
            }
        }

        $output .= $this->view->form()->closeTag($form);

        return $output;
    }
}
