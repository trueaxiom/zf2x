<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl;

use Zend\Permissions\Acl\Acl;
use Zend\ServiceManager\FactoryInterface;
use Zend\Permissions\Acl\Role\GenericRole;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Permissions\Acl\Assertion\AssertionInterface;

/**
 * Acl Factory
 */
class Factory implements FactoryInterface
{
    /**
     * Create, configure and return an instance of Acl
     *
     * @param  ServiceLocatorInterface $locator
     * @return Zend\Permissions\Acl\Acl
     */
    public function createService(ServiceLocatorInterface $locator)
    {
        $acl = new Acl;
        $options = $locator->get('Acl\Options');
        //Add Roles
        foreach ($options->getRoles() as $role => $parents) {
            $acl->addRole(new GenericRole($role), $parents);
        }
        if (!$acl->getRoles()) {
            $acl->addRole(new GenericRole("guest"));
        }
        //Add Resources
        foreach ($options->getResources() as $resource => $parent) {
            $acl->addResource(new GenericResource($resource), $parent);
        }
        //Add Deny rules
        foreach ($options->getDeny() as $rule) {
            list($role,$resource,$privilege,$assertion) = $rule;
            if ($assertion !== null && !$assertion instanceof AssertionInterface) {
                $assertion = $locator->get($assertion);
            }
            $acl->deny($role, $resource, $privilege, $assertion);
        }
        //Add Allow rules
        foreach ($options->getAllow() as $rule) {
            list($role,$resource,$privilege,$assertion) = $rule;
            if ($assertion !== null && !$assertion instanceof AssertionInterface) {
                $assertion = $locator->get($assertion);
            }
            if ($assertion) {
                throw new \Exception("testing required - unsure framework behaviour is correct");
                //var_dump($role, $resource, $privilege);
            }
            $acl->allow($role, $resource, $privilege, $assertion);
        }
        return $acl;
    }
}
