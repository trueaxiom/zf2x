<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Authentication;

use Zend\Authentication\Result;
use Zend\EventManager\EventManager;
use Zend\Stdlib\Hydrator\ClassMethods;
use ZF2x\Authentication\Entity\AuthEntity;
use ZF2x\Authentication\Entity\AuthEntityInterface;

use ZF2x\Identification\Event\IdentityEvent;
use Zend\EventManager\EventManagerInterface;
use Zend\Authentication\Adapter\AdapterInterface;
use ZF2x\Authentication\Identification\Identity\AuthIdentity;
use Zend\Authentication\AuthenticationService as ZendAuthService;

/**
 * Authentication Service
 */
class AuthenticationService extends ZendAuthService
{
    /**
     * @var EventManagerInterface|null
     */
    protected $eventManager;

    /**
     * Get Event Manager
     *
     * @return mixed EventManagerInterface|null
     */
    public function getEventManager()
    {
        if (null === $this->eventManager) {
            $this->setEventManager(new EventManager);
        }
        return $this->eventManager;
    }

    /**
     * Set Event Manager
     *
     * @param EventManagerInterface $eventManager Event Manager
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->eventManager = $eventManager;
    }

    /**
     * Identify with given adapter
     *
     * @param  IdentificationAdapterInterface $adapter Identification Adapter
     * @return Result                         Zend\Authentication\Result
     */
    public function authenticate(AdapterInterface $adapter)
    {
        $result = $adapter->authenticate();
        if (!$result->isValid()) {
            return $result;
        }
        $identity = $this->composeIdentity($result->getIdentity());
        $event = new IdentityEvent;
        $event->setIdentity($identity);
        $identities = $this->getEventManager()->trigger(IdentityEvent::UPDATE_EVENT, $event);
        $result = new Result($result->getCode(), $identities);

        return $result;
    }

    public function composeIdentity(AuthEntityInterface $entity)
    {
        $identity = new AuthIdentity;
        $hydrator = new ClassMethods;
        $data = $hydrator->extract($entity);
        $hydrator->hydrate($data, $identity);
        return $identity;
    }
}
