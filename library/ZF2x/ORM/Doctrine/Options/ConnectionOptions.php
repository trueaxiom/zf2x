<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\ORM\Doctrine\Options;

use Zend\Stdlib\AbstractOptions;

/**
 * Connection Options
 */
class ConnectionOptions extends AbstractOptions
{
    /**
     * Driver selection for database connection
     * See Doctrine\DBAL\DriverManager::_driverMap for avialable
     * driver config keys
     *
     * @var string
     */
    protected $driver   = 'pdo_mysql';

    /**
     * Database Host
     *
     * @var string
     */
    protected $host     = '127.0.0.1';

    /**
     * Database User
     *
     * @var string
     */
    protected $user     = 'root';

    /**
     * Database Password
     *
     * @var string
     */
    protected $password = '';

    /**
     * Database Name
     *
     * @var string
     */
    protected $dbname   = '';

    /**
     * Path to Database
     * For SQLite;
     *
     * @var string
     */
    protected $path     = '';

    /**
     * Gets the Driver selection for database connection
     *
     * @return string
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Sets the Driver selection for database connection
     *
     * @param string $driver the driver
     *
     * @return self
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Gets the Database Host.
     *
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Sets the Database Host.
     *
     * @param string $host the host
     *
     * @return self
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Gets the Database User.
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets the Database User.
     *
     * @param string $user the user
     *
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Gets the Database Password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the Database Password.
     *
     * @param string $password the password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Gets the Database Name.
     *
     * @return string
     */
    public function getDbname()
    {
        return $this->dbname;
    }

    /**
     * Sets the Database Name.
     *
     * @param string $dbname the dbname
     *
     * @return self
     */
    public function setDbname($dbname)
    {
        $this->dbname = $dbname;

        return $this;
    }

    /**
     * Gets the Path to Database For SQLite
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Sets the Path to Database For SQLite
     *
     * @param string $path the path
     *
     * @return self
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }
}
