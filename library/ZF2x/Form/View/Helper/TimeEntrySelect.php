<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Form\View\Helper;

use Zend\Form\ElementInterface;
use ZF2x\Form\Element\TimeEntrySelect as TimeSelectElement;
use Zend\Form\Exception;
use Zend\Form\View\Helper\AbstractHelper;

class TimeEntrySelect extends AbstractHelper
{
    /**
     * FormSelect helper
     *
     * @var FormSelect
     */
    protected $selectHelper;

    /**
     * Invoke helper as function
     *
     * Proxies to {@link render()}.
     *
     * @param  ElementInterface $element
     * @param  int              $dateType
     * @param  null|string      $locale
     * @return FormDateSelect
     */
    public function __invoke(ElementInterface $element = null)
    {
        if (!$element) {
            return $this;
        }

        return $this->render($element);
    }

    /**
     * Render a month element that is composed of two selects
     *
     * @param  \Zend\Form\ElementInterface $element
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @throws \Zend\Form\Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element)
    {

        if (!$element instanceof TimeSelectElement) {
            return '';
            throw new Exception\InvalidArgumentException(
                sprintf(
                    '%s requires that the element is of type Trueaxiom\Timesheet\Form\Element\TimeEntrySelect',
                    __METHOD__
                )
            );
        }

        $name = $element->getName();
        if ($name === null || $name === '') {
            throw new Exception\DomainException(
                sprintf(
                    '%s requires that the element has an assigned name; none discovered',
                    __METHOD__
                )
            );
        }

        $element->getHourElement()->setValueOptions($this->getHourOptions());
        $element->getMinuteElement()->setValueOptions($this->getMinuteOptions());

        $element->getHourElement()->setAttribute('class', 'hours');
        $element->getMinuteElement()->setAttribute('class', 'minutes');

        $markup = $this->getSelectElementHelper()->render($element->getHourElement()).' '.
            $this->getSelectElementHelper()->render($element->getMinuteElement());

        return $markup;
    }

    protected function getHourOptions()
    {
        $options = array();
        for ($i=0; $i<24; $i++) {
            $options[$i] = "{$i} Hours";
        }
        return $options;
    }

    protected function getMinuteOptions()
    {
        $options = array();
        for ($i=0; $i<60; $i=$i+5) {
            $options[$i] = "{$i} Mins";
        }
        return $options;
    }

    /**
     * Retrieve the FormSelect helper
     *
     * @return FormSelect
     */
    protected function getSelectElementHelper()
    {
        if ($this->selectHelper) {
            return $this->selectHelper;
        }

        if (method_exists($this->view, 'plugin')) {
            $this->selectHelper = $this->view->plugin('formselect');
        }

        return $this->selectHelper;
    }
}
