<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Identity;

use ZF2x\Permissions\Acl\Options;
use ZF2x\Permissions\Acl\Identity\AclIdentity;
use ZF2x\Permissions\Acl\Mapper\RoleEntityConfigMapper;
use ZF2x\Permissions\Acl\Identity\AclIdentityInterface;
use ZF2x\Identification\Manager\AbstractIdentityManager;

class AclIdentityManager extends AbstractIdentityManager
{
    const IDENTITY_CLASS = 'ZF2x\Permissions\Acl\Identity\AclIdentity';
    const SESSION_NAMESPACE = 'acl';

    /**
     * @var Options
     */
    protected $options;

    /**
     * Get Options
     *
     * @return Options|null
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set Options
     *
     * @param Options $options
     */
    public function setOptions(Options $options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get Identity
     *
     * @return AclIdentity
     */
    public function getIdentity()
    {
        $identity = parent::getIdentity();
        if (!$identity instanceof AclIdentityInterface) {
            $identity = new AclIdentity;
        }
        if ($identity->getRole() === null) {
            $options = $this->getOptions();
            $defaultRole = $options->getDefaultRole();
            $identity->setRole($defaultRole);
        }
        return $identity;
    }
}
