<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Mail\Transport;

use Zend\Stdlib\AbstractOptions;

class SendmailOptions extends AbstractOptions
{
    /**
     * Default From Name
     *
     * Used to populate the additional_parameters argument to mail()
     *
     * @var array
     */
    protected $parameters = array();

    /**
     * Gets the Default From Name
     *
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Sets the Default From Name
     *
     * @param array $parameters the parameters
     *
     * @return self
     */
    public function setParameters(array $parameters = array())
    {
        $this->parameters = $parameters;

        return $this;
    }
}
