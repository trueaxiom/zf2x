<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\ORM\Doctrine\Events\SharedEventProxy;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;

class Module implements BootstrapListenerInterface
{
    public function onBootstrap(EventInterface $event)
    {
        $app = $event->getApplication();
        $sl = $app->getServiceManager();
        $doctrine = $sl->get('DoctrineEntityManager');
        $doctrineEvm = $doctrine->getEventManager();
        $subscriber = new Subscriber;
        $doctrineEvm->addEventSubscriber($subscriber);
    }
}
