<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\ORM\Doctrine;

use Zend\Mvc\MvcEvent;
use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Doctrine ORM Module
 */
class Module implements ConfigProviderInterface
{
    /**
     * Get Configuration
     * @return array configuration
     */
    public function getConfig()
    {
        return array(
            'service_manager' => array(
                'initializers' => array(
                    'ZF2x\ORM\Doctrine\DoctrineORMAwareInitializer',
                ),
                'factories' => array(
                    'DoctrineORMConnectionOptions'      => 'ZF2x\ORM\Doctrine\Options\ConnectionOptionsFactory',
                    'DoctrineORMConfigurationOptions'   => 'ZF2x\ORM\Doctrine\Options\ConfigurationOptionsFactory',
                    'DoctrineORMConfiguration'          => 'ZF2x\ORM\Doctrine\Factory\ConfigurationFactory',
                    'DoctrineEntityManager'             => 'ZF2x\ORM\Doctrine\Factory\EntityManagerFactory',
                ),
            ),
            'hydrators' => array(
                'initializers' => array(
                    'ZF2x\ORM\Doctrine\DoctrineORMAwareInitializer',
                ),
                'invokables' => array(
                    'DoctrineORMEntityHydrator' => 'ZF2x\ORM\Doctrine\Stdlib\Hydrator\EntityHydrator',
                ),
            ),
        );
    }
}
