<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Authentication\Mapper\AuthEntity\Config;

use ZF2x\Stdlib\ArrayUtils;
use ZF2x\Stdlib\AbstractOptionsFactory;

/**
 * Factory for Acl Options
 */
class OptionsFactory extends AbstractOptionsFactory
{
    /**
     * Path from which to retrieve configuration
     *
     * @const string Configuration Path
     */
    const CONFIG_PATH = 'identification';

    /**
     *
     */
    const OPTIONS_CLASS = 'ZF2x\Authentication\Mapper\AuthEntity\Config\Options';
}
