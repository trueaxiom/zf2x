<?php
chdir(__DIR__);
$path = '';
while (!file_exists($path.'vendor')) {
    $path .= '../';
}
$phpunit_bin      = __DIR__ . '/' . $path . 'vendor/bin/phpunit';
$phpunit_bin      = file_exists($phpunit_bin) ? $phpunit_bin : 'phpunit';
$phpunit_conf     = 'phpunit.xml';
$phpunit_opts     = "-c $phpunit_conf";
system("$phpunit_bin $phpunit_opts", $result);
exit($result);
