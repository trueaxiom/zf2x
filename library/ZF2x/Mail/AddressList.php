<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Mail;

use Zend\Mail\AddressList as ZendAddressList;

class AddressList extends ZendAddressList
{
    public function __construct(array $addresses = array())
    {
        $this->addMany($addresses);
    }
}
