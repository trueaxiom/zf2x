<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Form\View\Helper\FormElement;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return array(
            'view_helpers' => array(
                'factories' => array(
                    'form_element' => function ($sl) {
                        $sl = $sl->getServiceLocator();
                        $helper = new \ZF2x\Form\View\Helper\FormElement\FormElement;
                        $options = $sl->get('FormElementHelperOptions');
                        $helper->setOptions($options);
                        return $helper;
                    },
                ),
            ),
            'service_manager' => array(
                'factories' => array(
                    'FormElementHelperOptions' => 'ZF2x\Form\View\Helper\FormElement\OptionsFactory',
                ),
            ),
        );
    }
}
