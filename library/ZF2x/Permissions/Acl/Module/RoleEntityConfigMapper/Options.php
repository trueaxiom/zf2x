<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Module\RoleEntityConfigMapper;

use Zend\Stdlib\AbstractOptions;

class Options extends AbstractOptions
{
    /**
     * Identities
     * @var array
     */
    protected $identities = array();

    /**
     * Gets the value of identities.
     *
     * @return mixed
     */
    public function getIdentities()
    {
        return $this->identities;
    }

    /**
     * Sets the value of identities.
     *
     * @param mixed $identities the identities
     *
     * @return self
     */
    public function setIdentities($identities)
    {
        $this->identities = $identities;

        return $this;
    }
}
