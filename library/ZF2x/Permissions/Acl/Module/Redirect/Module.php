<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Permissions\Acl\Module\Redirect;

use Zend\EventManager\EventInterface;
use ZF2x\Permissions\Acl\Event\AclEvent;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use ZF2x\Permissions\Acl\Module\Redirect\Listener\DenyRouteRedirectListener;

/**
 * Acl Redirect Module
 */
class Module implements ConfigProviderInterface
{

    public function onBootstrap(EventInterface $e)
    {
        $application    = $e->getApplication();
        $sl             = $application->getServiceManager();
        $em             = $application->getEventManager();
        $sm             = $em->getSharedManager();
        //Deny Route Redirect Listener
        $sm->attach(
            '*',
            AclEvent::DENY_ROUTE_EVENT,
            function ($e) use ($sl) {
                $listener = $sl->get('AclRedirectListener');
                return $listener->onDenyRoute($e);
            }
        );
    }

    public function getConfig()
    {
        return array(
            'service_manager' => array(
                'factories' => array(
                    'AclRedirectOptions' => 'ZF2x\Permissions\Acl\Module\Redirect\OptionsFactory',
                    'AclRedirectListener' => function ($sl) {
                        $listener = new DenyRouteRedirectListener;
                        $options = $sl->get('AclRedirectOptions');
                        $listener->setOptions($options);
                        return $listener;
                    },
                ),
            ),
        );
    }
}
