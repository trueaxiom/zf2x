<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Authentication\Adapter;

use Exception;
use Zend\Authentication\Result;
use Zend\Crypt\Password\PasswordInterface;
use Zend\Authentication\Adapter\AbstractAdapter;
use ZF2x\Authentication\Entity\AuthEntityInterface;
use ZF2x\Authentication\Mapper\AuthEntity\AuthEntityMapperInterface;

class PasswordAdapter extends AbstractAdapter
{
    /**
     * @var PasswordInterface
     */
    protected $passwordService;

    /**
     * @var AuthEntityMapperInteface
     */
    protected $mapper;

    /**
     * Identify
     *
     * @return Result Authentication Result
     */
    public function authenticate()
    {
        $identity   = $this->getIdentity();
        $credential = $this->getCredential();
        $mapper     = $this->getMapper();
        $auth       = $mapper->findOneByIdentity($identity);
        if (!$auth) {
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null);
        }
        if (!$auth instanceof AuthEntityInterface) {
            throw new Exception("Auth Mapper Result must implement AuthEntityInterface");
        }
        $passwordService    = $this->getPasswordService();
        $authCredential     = $auth->getCredential();
        $valid              = $passwordService->verify($credential, $authCredential);
        if (!$valid) {
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null);
        }
        return new Result(Result::SUCCESS, $auth);
    }

    /**
     * Gets the value of passwordService.
     *
     * @return PasswordInterface
     */
    public function getPasswordService()
    {
        return $this->passwordService;
    }

    /**
     * Sets the value of passwordService.
     *
     * @param PasswordInterface $passwordService the password service
     *
     * @return self
     */
    public function setPasswordService(PasswordInterface $passwordService)
    {
        $this->passwordService = $passwordService;

        return $this;
    }

    /**
     * Gets the value of mapper.
     *
     * @return AuthEntityMapperInteface
     */
    public function getMapper()
    {
        return $this->mapper;
    }

    /**
     * Sets the value of mapper.
     *
     * @param AuthEntityMapperInteface $mapper the mapper
     *
     * @return self
     */
    public function setMapper(AuthEntityMapperInterface $mapper)
    {
        $this->mapper = $mapper;

        return $this;
    }
}
