<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Navigation\AbstractFactory;

use ZF2x\Stdlib\ArrayUtils;
use Zend\Navigation\Navigation;
use ZF2x\Navigation\Page\ZF2xPage;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\AbstractFactoryInterface;

/**
 * Navigation Container Abstract Factory
 */
class AbstractNavigationFactory implements AbstractFactoryInterface
{
    /**
     * Can Create Container ?
     * @param  ServiceLocatorInterface $serviceLocator Service Locator
     * @param  string                  $name           Name
     * @param  string                  $requestedName  Request Name
     * @return boolean
     */
    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        return ArrayUtils::getPath($serviceLocator->get('config'), 'navigation.'.$name) ? true : false;
    }

    /**
     * Create Container
     * @param  ServiceLocatorInterface $serviceLocator Service Locator
     * @param  string                  $name           container alias
     * @param  string                  $requestedName  container alias
     * @return Navigation
     */
    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $pages = ArrayUtils::getPath($serviceLocator->get('config'), 'navigation.'.$name);
        if (!$pages) {
            $pages = array();
        }
        $application = $serviceLocator->get('Application');
        $router = $application->getMvcEvent()->getRouter();
        $routeMatch = $application->getMvcEvent()->getRouteMatch();
        $setup = function ($pages, $router, $routeMatch) use (&$setup) {
            foreach ($pages as &$page) {
                $page['router'] = $router;
                if ($routeMatch) {
                    $page['routeMatch'] = $routeMatch;
                }
                if (isset($page['pages'])) {
                    $page['pages'] = $setup($page['pages'], $router, $routeMatch);
                }
                $page['type'] = 'ZF2x\Navigation\Page\ZF2xPage';
            }
            return $pages;
        };

        return new Navigation($setup($pages,$router,$routeMatch));
    }
}
