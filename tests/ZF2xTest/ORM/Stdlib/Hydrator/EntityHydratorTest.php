<?php
namespace ZF2xTest\ORM\Doctrine\Stdlib\Hydrator;

use Asset;
use ReflectionClass;
use ZF2x\ORM\Doctrine\Stdlib\Hydrator\EntityHydrator;

class EntityHydratorTest extends \PHPUnit_Framework_TestCase
{
    protected $hydrator;

    protected $metadata;

    protected $entityManager;

    public function setup()
    {
        parent::setUp();
        $this->hydrator = new EntityHydrator;
        $this->metadata = $this->getMock('Doctrine\Common\Persistence\Mapping\ClassMetadata');
        $this->entityManager = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
        $this->entityManager
            ->expects($this->any())
            ->method('getClassMetadata')
            ->will($this->returnValue($this->metadata));
    }

    public function configureMetadataForSimpleEntity()
    {
        $reflection = new ReflectionClass('ZF2xTest\ORM\Doctrine\Stdlib\Hydrator\Asset\SimpleEntity');
        $this->metadata
            ->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('ZF2xTest\ORM\Doctrine\Stdlib\Hydrator\Asset\SimpleEntity'));
        $this->metadata
            ->expects($this->any())
            ->method('getAssociationNames')
            ->will($this->returnValue(array()));
        $this->metadata
            ->expects($this->any())
            ->method('getFieldNames')
            ->will($this->returnValue(array('id', 'name')));
        $this->metadata
            ->expects($this->any())
            ->method('hasAssociation')
            ->will($this->returnValue(false));
        $this->metadata
            ->expects($this->any())
            ->method('getIdentifierFieldNames')
            ->will($this->returnValue(array('id')));
        $this->metadata
            ->expects($this->any())
            ->method('getReflectionClass')
            ->will($this->returnValue($refl));
    }

    public function testCanExtractSimpleEntity()
    {
        $this->configureMetadataForSimpleEntity();
        $entity = new Asset\SimpleEntity;
        $entity->setId(1);
        $entity->setName('testCanExtractSimpleEntity');
        $data = $this->hydrator->extract($entity);
        $this->assertEquals(array('id' => 1, 'name' => 'testCanExtractSimpleEntity'));
    }
}
