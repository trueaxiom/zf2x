<?php
/**
 * zf2x (http://zf2x.trueaxiom.co.uk/)
 *
 * @link      http://github.com/trueaxiom/zf2x for the canonical source repository
 * @copyright Copyright (c) 2013 True Axiom Limited UK. (http://www.trueaxiom.co.uk)
 * @license   http://www.opensource.org/licenses/mit-license.html  MIT License
 */

namespace ZF2x\Mail\Transport;

use ZF2x\Stdlib\AbstractOptionsFactory;

class FileOptionsFactory extends AbstractOptionsFactory
{
    /**
     * @var string Path from which to retrieve configuration
     */
    protected $configurationPath = 'zf2x.mail.transport.file';

    /**
     * @var string FQCN of Options to hydrate
     */
    protected $optionsClass = 'Zend\Mail\Transport\FileOptions';
}
